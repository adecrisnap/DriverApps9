import { StyleSheet, View, Alert } from "react-native";
import React from "react";
import {
  Divider,
  Text,
  Avatar,
  Headline,
  Title,
  Subheading,
  Caption,
  Card,
  List,
  Chip,
  Button,
  useTheme
} from "react-native-paper";
import Gap from "./Gap";
import {
  // GET_PLANNER_CUSTOMERDATA_BY_DELIVERYPLAN,
  GET_DONUMBER_BY_PLAN_AND_CUSTOMER,
  GET_STATUS_CHECKINOUT_DRIVER,
  NGROK_URL,
  POST_LOCATION_DATA,
  showError,
  USER_ALREADY_CHECKOUT,
  USER_ALREADY_CHECKIN,
  showSuccess,
  SUCCESS_CHECKIN,
  FAILED_CHECKIN,
  SUCCESS_CHECKOUT,
  FAILED_CHECKOUT,
  STATUS_DO_INIT,
  GET_STATUS_CICO_TODAY,
} from "../utils/Constants";
import axios from "axios";
import ChipInformationStatus from "./ChipInformationStatus";
import { Platform } from "expo-modules-core";
import { selectUserId } from "../redux/slices/authSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import * as ExpoLocation from "expo-location";
import { selectIsLoading, setLoading, setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import { getDataObjectNonSecured, getDataStringNonSecured, removeFilterAsyncStorage, removeKey, storeDataObjectNonSecured, storeDataStringNonSecured } from "../utils/LocalStorage";
import { selectIsAlreadyCheckInSomewhere, setcheckInOutFalse, setcheckInOutTrue } from "../redux/slices/checkInOutSlice";
import { PreferencesContext } from "../utils/PreferencesContext";

import * as BackgroundFetch from 'expo-background-fetch';
import * as TaskManager from 'expo-task-manager';
import { axiosInstance } from "../config/axiosConfig";


const CustomerDeliveryCard = ({
  id,
  nama,
  custid,
  deliveryplanid,
  navigation,
  route,
  address,
  original_latitude,
  original_longitude,
  status_delivery_id,
  status_description,
  id_rute,
}) => {
  const [alreadyCheckIn, setAlreadyCheckIn] = React.useState(false);
  const [expanded, setExpanded] = React.useState(true);
  const [statusCico, setStatusCico] = React.useState("Check In");
  const dispatch = useDispatch();
  const handlePress = () => setExpanded(!expanded);
  const [doNumbers, setDONumbers] = React.useState([]);
  const uid = useSelector(selectUserId);

  const alreadyCheckInSomewhere  = useSelector(selectIsAlreadyCheckInSomewhere);

  const [confirmableCheckOut, setConfirmableCheckOut] = React.useState(false);

  const [buttonCheckInDisabled, setButtonCheckInDisabled] = React.useState(false);
  const [buttonCheckOutDisabled, setButtonCheckOutDisabled] = React.useState(true);

  const [idCheckIn, setIdCheckIn] = React.useState(0);
  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);

  const BACKGROUND_FETCH_TASK = 'background-fetch';
  

  const showAlertOut = async () => { return new Promise((resolve, reject) => {
  Alert.alert(
    "Konfirmasi Check Out ",
    "Yakin mau checkout",
    [
      {
        text: "Cancel",
        onPress: () => {
          resolve(false)
        },
        style: "cancel",
      },
      {
        text: "OK", 
        onPress: () => {
          resolve(true)
        },
      }
    ],
    {
      cancelable: true,
      onDismiss: () => {
        resolve (false)
      },
      /* onDismiss: () =>
        Alert.alert(
          "This alert was dismissed by tapping outside of the alert dialog."
        ), */
    }
  )
});
  }

  const needConfirmationCheckOut = () => {
      Alert.alert(
        "Alert Title",
        "My Alert Msg",
        [
          {
            text: "Cancel",
            //onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          { text: "OK", 
            //onPress: () => console.log("OK Pressed")
          }
        ],
        {
          cancelable: true,
          onDismiss: () => console.log("Dismissed Pressed"),
        }
      );
    }
  
  const onPressSignInOut = async () => {
  
    dispatch(setLoadingTrue());
    let permissionStatus = null;
    //console.log(permissionStatus);
    if (Platform.OS === "ios") {
      let { status } = await Permissions.askAsync(Permissions.LOCATION);
      permissionStatus = status;
    } else {
      let { status } = await ExpoLocation.requestForegroundPermissionsAsync();
      permissionStatus = status;
    }
    if (permissionStatus !== "granted") {
      dispatch(setLoadingFalse());
      let { status } =  await ExpoLocation.requestForegroundPermissionsAsync();
      permissionStatus = status;
      showError("Please Allow Location");
      return;
    }

    //console.log(permissionStatus);

    let currentLocation = await ExpoLocation.getCurrentPositionAsync({
      enableHighAccuracy: true,
    });

    const date_iso = new Date().toISOString();

    currentLocation.user_id = uid;
    currentLocation.location_id = custid;
    currentLocation.location_name = nama;
    
    currentLocation.location_latitude_coordinate = original_latitude;
    currentLocation.location_longitude_coordinate = original_longitude;
    currentLocation.id_self_in = idCheckIn;
    currentLocation.date_isoformat = date_iso;
    currentLocation.customerlocation_refnumber = id_rute;

    alreadyCheckIn
      ? (currentLocation.STATE_INOUT = "OUT")
      : (currentLocation.STATE_INOUT = "IN");

    if (statusCico === "Check In") {
     
      //console.log(currentLocation)

      if (alreadyCheckInSomewhere){
        showError(USER_ALREADY_CHECKIN);
        dispatch(setLoadingFalse());
        return;
      }
      
      //.await axios
      await axiosInstance
      .post(NGROK_URL + POST_LOCATION_DATA, {
        data: currentLocation,
      })
      .then(function (response) {
        setIdCheckIn(response.data?.results.data)
        setButtonCheckInDisabled(true);
        setButtonCheckOutDisabled(false);
        setAlreadyCheckIn(true);
        setStatusCico("Check Out"); 
        dispatch(setcheckInOutTrue());
        showSuccess(SUCCESS_CHECKIN);

        let x = {
          customer_id: custid,
          id_check_in: response.data?.results.data,
          status_check_in: "Check In"
        }

        storeDataObjectNonSecured(custid + "-STATUSCICO",x);
      })
      .catch(function (error) {
        console.log(error);
        showError(FAILED_CHECKIN);
      });
      dispatch(setLoadingFalse());
      return;
    }

    if (statusCico === "Check Out") {
      dispatch(setLoadingFalse())
      let checkout = await showAlertOut();
      dispatch(setLoadingTrue());
      if (checkout) {
        await axios
        .post(NGROK_URL + POST_LOCATION_DATA, {
          data: currentLocation,
        })
        .then(function (response) {
          setIdCheckIn(0)
          setButtonCheckInDisabled(true);
          setButtonCheckOutDisabled(true);
          setAlreadyCheckIn(false);
          setStatusCico("Completed");     
          dispatch(setcheckInOutFalse());
          showSuccess(SUCCESS_CHECKOUT);  
          removeFilterAsyncStorage('TOLAK');

          let x = {
            customer_id: custid,
            id_check_in: 0,
            status_check_in: "Check Out"
          }
  
          storeDataObjectNonSecured(custid + "-STATUSCICO",x);
        })
        .catch(function (error) {
          showError(FAILED_CHECKOUT);
        });
      } else {
        //console.log("Anda membatalkan check out");
      }
      dispatch(setLoadingFalse());
      return;
    }
  };


  const checkStatusInOut = async () => {
    console.log('cek status in out')
    let t = await getDataObjectNonSecured(custid + "-STATUSCICO");
    //console.log(t);

    
    if (t === null) {
      try {
        console.log('ganti hari');
        
        let statusInOut =  await axios
            .get(NGROK_URL + GET_STATUS_CICO_TODAY + '/' + custid + '/' + uid)

        console.log('data inout:', statusInOut.data.results.data.length);

        if (statusInOut.data.results.data.length === 0)
        {
          console.log('no data, mungkin bbeda hari ');
          removeKey(custid + "-STATUSCICO");
          return null;
        } else {

          if (statusInOut.data?.results.data[0].status_inout === "IN"){

            let x = {
              customer_id: custid,
              id_check_in: statusInOut.data?.results.data[0].id,
              status_check_in: "Check In"
            }

            await storeDataObjectNonSecured(custid + "-STATUSCICO",x);
          }

          if (statusInOut.data?.results.data[0].status_inout === "OUT"){
            let x = {
              customer_id: custid,
              id_check_in: statusInOut.data?.results.data[0].id,
              status_check_in: "Check Out"
            }

            await storeDataObjectNonSecured(custid + "-STATUSCICO",x);
          }
        }

      } catch (error) {
        //console.log(error);
      }
      
        
    }

    t = await getDataObjectNonSecured(custid + "-STATUSCICO");

    if (t?.status_check_in === "Check In") {
        setIdCheckIn(t?.id_check_in)
        setButtonCheckInDisabled(true);
        setButtonCheckOutDisabled(false);
        setAlreadyCheckIn(true);
        setStatusCico("Check Out"); 
        dispatch(setcheckInOutTrue());
        //showSuccess(SUCCESS_CHECKIN);
        //storeDataStringNonSecured(custid + "-STATUSCICO","Check In");

        let x = {
          customer_id: t?.customer_id,
          id_check_in: t?.id_check_in,
          status_check_in: "Check In"
        }

        storeDataObjectNonSecured(custid + "-STATUSCICO",x);

    } 

    if (t?.status_check_in === "Check Out") {
      setIdCheckIn(0)
      setButtonCheckInDisabled(true);
      setButtonCheckOutDisabled(true);
      setAlreadyCheckIn(false);
      setStatusCico("Completed");     
      dispatch(setcheckInOutFalse());

      let x = {
        customer_id: t?.customer_id,
        id_check_in: 0,
        status_check_in: "Check Out"
      }

      storeDataObjectNonSecured(custid + "-STATUSCICO",x);

  } 

  }

 
  // const openDOKonfirmasiPenerimaanScreen = () => r{
  //   //console.log("Pressed");
  //   navigation.navigate("KonfirmasiPenerimaanScreen", {
  //     customer_id: custid,
  //     customer_name: nama,
  //     deliveryplan_id: deliveryplanid,
  //   });

  //   /* navigation.navigate('Details', {
  //     itemId: 86,
  //     otherParam: 'anything you want here',
  //   }); */
  // };

  const openScreenReturNonPlanning= () => {
      navigation.navigate("ReturnNonScheduled", {
      customer_id: custid,
      customer_name: nama,
      deliveryplan_id: deliveryplanid,
    });
  };

  const openScreenReturPlanning= () => {
    navigation.navigate("ReturScreen", {
      customer_id: custid,
    });
};


  const openScreenRekapPengirimanBarang = () => {
    navigation.navigate("RekapPengirimanBarangScreen", {
      rute_id : id_rute,
      customer_id: custid,
      sedang_cekin: alreadyCheckIn,
      status_description: status_description,
    });
  }

  const openScreenPettyCashPerCustomer = () => {
    navigation.navigate("PettyCashPerCustomerScreen", {
      rute_id : id_rute,
    });
  }



  // React.useEffect(() => {
  //   let isSubscribed = true;
  //   fetchDataDOByPlanAndCustomer(custid, deliveryplanid);
  //   return () => (isSubscribed = false);
  // }, [id]);

  React.useEffect(() => {
     checkStatusInOut();
   }, []);

  const toggleFetchTask = async () => {
    if (isRegistered) {
      await unregisterBackgroundFetchAsync();
    } else {
      await registerBackgroundFetchAsync();
    }

    checkStatusTaskAsync();
  };

  const checkStatusTaskAsync = async () => {
    const status = await BackgroundFetch.getStatusAsync();
    const isRegistered = await TaskManager.isTaskRegisteredAsync(BACKGROUND_FETCH_TASK);
    setStatus(status);
    setIsRegistered(isRegistered);
  };

  const fetchDataDOByPlanAndCustomer = React.useCallback(
    async (custid, deliveryplanid) => {
      axios
        .get(
          NGROK_URL +
            GET_DONUMBER_BY_PLAN_AND_CUSTOMER +
            "/" +
            deliveryplanid +
            "/" +
            custid
        )
        .then(function (response) {
          setDONumbers(response.data);
        })
        .catch(function (error) {
          //console.log("ERROR : ", error);
        });
    },
    []
  );

  return (
    <Card style={styles.customerCardBackground}>
      <Card.Title
        title={nama}
        titleStyle={styles.titleStyle}
        subtitle={address}
        subtitleStyle={styles.subtitleStyle}
        left={(props) => <Avatar.Text size={24} label={id + 1} />}
        //right={(props) => (
        //  <Subheading style={styles.titleText} onPress={onPressSignInOut}>
        //    {statusCico}
        //  </Subheading>
        //)}
      />
      <Card.Content>
        <View style={[{ flexDirection: "row"  } ]}>
          <Caption>Rute : </Caption>
          <Caption>{id_rute}</Caption>
        </View>

        <View style={[{ flexDirection: "row"  } ]} >
            <Caption>Lat : </Caption>
            <Caption>{original_latitude}</Caption>
            <Gap width={10} />
            <Caption>Long : </Caption>
            <Caption>{original_longitude}</Caption>
            <Gap width={80} />
            <Caption style ={[{ color:theme.colors.error } ]}>{status_description}</Caption>
        </View>
        <View style={[{ flexDirection: "row"  } ]} >
          <View>
          <Button mode="text" labelStyle={[{ textTransform: "capitalize"  } ]}  onPress={onPressSignInOut} disabled={buttonCheckInDisabled} >
            Check In
          </Button>
          </View>
          <View>
          <Button mode="text" labelStyle={[{ textTransform: "capitalize"  } ]}  onPress={onPressSignInOut} disabled={buttonCheckOutDisabled} >
            Check Out
          </Button>
          </View>
          <View>
          <Button mode="text" labelStyle={[{ textTransform: "capitalize"  } ]}  onPress={openScreenPettyCashPerCustomer} >
            Petty Cash
          </Button>
          </View>
          </View>
        <View>
        <Button mode="outlined" compact="true" color="#00B666" onPress={openScreenRekapPengirimanBarang}>
          Rekap Barang
        </Button>

       {/*  <Button mode="outlined" compact="true" color="#CCB666" onPress={openScreenReturNonPlanning}>
          Retur Barang Non Planning
        </Button> */}

        <Button mode="outlined" compact="true" color="#CCA666" onPress={openScreenReturPlanning}>
          Ambil Retur
        </Button>
        </View>
        
        {/* <List.Accordion
          title="Cek Barang Kiriman"
          expanded={expanded}
          onPress={handlePress}
        > */}
          {/* <List.Item
            title="First Item"
            description="Item description"
            left={props => <List.Icon {...props} icon="folder" />}
            right={props => <List.Icon {...props} icon="arrow-right" />}
        /> */}
          
        {/* </List.Accordion> */}
       
      </Card.Content>
    </Card>

    /* <View style={styles.container}>
      <View style={styles.titleContainer}>
        <View style={styles.itemContainer}>
            <Avatar.Text size={24} label={id} />
            <Gap width={10}></Gap>
            <Caption>{nama}</Caption>
        </View>
        <View style={styles.signInOutContainer}>
            <Subheading style={styles.titleText} onPress={onPressSignInOut}>
                Sign In
            </Subheading>
        </View>
      </View>

      <View>
        <Divider />
      </View>
    </View> */
  );
};

export default CustomerDeliveryCard;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    flexDirection: "column",
    justifyContent: "center", //Centered vertically
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
  },
  itemContainer: {
    flexDirection: "row",
  },
  titleText: {
    color: "#2AC02C",
    marginRight: 16,
  },
  titleStyle: {
    fontWeight: "900",
    textTransform: "uppercase",
    fontSize: 12,
  },
  subtitleStyle: {
    fontWeight: "400",
    textTransform: "capitalize",
    fontSize: 12,
  },
  signInOutContainer: {},
  customerCardBackground:{
    marginTop: 10
  }
});

import React, { memo, useCallback, useRef, useState } from 'react'
import { Dimensions, View, Platform } from 'react-native'
import { Button, Text, useTheme } from 'react-native-paper'
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown'
import axios from 'axios'
import Feather from 'react-native-vector-icons/Feather'
Feather.loadFont()


const AutoCompleteDropDownModif = memo(({url_locator, zIndex, filter_id, filter_by,parentCallback}) => {

  const theme = useTheme()
  const [loading, setLoading] = useState(false)
  const [suggestionsList, setSuggestionsList] = useState(null)
  const [selectedItem, setSelectedItem] = useState(null)
  const dropdownController = useRef(null)

  const searchRef = useRef(null)
  //console.log(url_locator);

  const url_locator_end = url_locator === "" ? 'https://jsonplaceholder.typicode.com/posts' : url_locator;



  const getSuggestions = useCallback(async q => {
    const filterToken = q.toLowerCase()
    //console.log('getSuggestions', q)
    if (typeof q !== 'string' || q.length < 3) {
      setSuggestionsList(null)
      return
    }
    setLoading(true)
    //const response = await fetch(url_locator)
    //console.log('url end :', url_locator_end);
    try {
      // const response = await fetch(url_locator_end)
      // console.log(response);
      // const items = await response.json();
      const response =  await axios
            .get(url_locator_end)

        //console.log('response:', response.data);
        const items = response.data;

        const suggestions = items
        .filter(item => item[filter_by].toLowerCase().includes(filterToken))
        .map(item => ({
                   id: item[filter_id],
                   title: item[filter_by],
        }))
        setSuggestionsList(suggestions)
        setLoading(false)
    } catch (error) {
      console.log(error);
      setLoading(false)
    }
    
  }, [])

  const onClearPress = useCallback(() => {
    
    setSuggestionsList(null)
  }, [])

  const onOpenSuggestionsList = useCallback(isOpened => {}, [])

  return (
    <>
      <View
        style={[
          // { flex: 1, flexDirection: 'row'},
          Platform.select({ ios: { zIndex: zIndex } }),
        ]}>
        <AutocompleteDropdown
          ref={searchRef}
          controller={controller => {
            dropdownController.current = controller
          }}
          // initialValue={'1'}
          direction={Platform.select({ ios: 'down' })}
          dataSet={suggestionsList}
          onChangeText={getSuggestions}
          
          onSelectItem={item => {
            //item && setSelectedItem(item.id)
            item && setSelectedItem(item)
            parentCallback(item);
          }}
          debounce={600}
          suggestionsListMaxHeight={Dimensions.get('window').height * 0.4}
          onClear={onClearPress}
          //  onSubmit={(e) => onSubmitSearch(e.nativeEvent.text)}
          onOpenSuggestionsList={onOpenSuggestionsList}
          loading={loading}
          useFilter={false} // set false to prevent rerender twice
          textInputProps={{
            placeholder: 'Ketik 3 huruf pertama..',
            autoCorrect: false,
            autoCapitalize: 'none',
            style: {
              borderRadius: 15,
              backgroundColor: theme.colors.background,
              color: theme.colors.text,
              paddingLeft: 18,
            },
          }}
          rightButtonsContainerStyle={{
            right: 8,
            height: 30,
            alignSelf: 'center',
          }}
          inputContainerStyle={{
            backgroundColor:  theme.colors.background,
            borderRadius: 15,
          }}
          suggestionsListContainerStyle={{
            backgroundColor:  theme.colors.background,
          }}
          containerStyle={{ flexGrow: 1, flexShrink: 1 }}
          renderItem={(item, text) => <Text style={{ color: theme.colors.text, padding: 15, }}>{item.title}</Text>}
          ChevronIconComponent={<Feather name="chevron-down" size={20} color={theme.colors.text} />}
          ClearIconComponent={<Feather name="x-circle" size={18} color={theme.colors.text} />}
          inputHeight={50}
          showChevron={false}
          closeOnBlur={false}
          //  showClear={false}
        />
        <View style={{ width: 10 }} />
       {/*  <Button style={{ flexGrow: 0 }} mode="text" onPress={() => dropdownController.current.toggle()}>
          Toggle 
        </Button> */}
        </View>
      {/* <Text style={{ color: '#668', fontSize: 13 }}>Selected item id: {JSON.stringify(selectedItem)}</Text> */}
    </>
  )
})

export default AutoCompleteDropDownModif
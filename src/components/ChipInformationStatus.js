import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Chip, useTheme, Colors  } from 'react-native-paper'
import { PreferencesContext } from '../utils/PreferencesContext'


const ChipInformationStatus = ({info}) => {

  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);

  const colorinfo = (info) => {
    switch(info) {
      case "Dalam Proses":   return Colors.yellowA400;
      case "Selesai Pengantaran - Fulfilled":   return Colors.greenA400;
      case "Selesai Pengantaran - Partial":   return Colors.green400;
      case "Tidak Terkirim":   return Colors.redA400;
      default : Colors.amber100;
    }
  }

  return (
      //<Chip icon="information" mode='outlined' style={[{ backgroundColor: theme.colors.primary }]} >
      <Chip icon="information" mode='outlined' style={[{ backgroundColor: colorinfo(info) }]} textStyle={[{ color: Colors.blueGrey900 }]} >
        {info}
      </Chip>
  )
}

export default ChipInformationStatus

const styles = StyleSheet.create({
})
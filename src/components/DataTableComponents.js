import { StyleSheet, View, FlatList } from 'react-native'
import React from 'react'
import { DataTable, Text } from 'react-native-paper';

const optionsPerPage = [2, 3, 4];

const DataTableComponents = ({data}) => {

  const [page, setPage] = React.useState(0);
  const [itemsPerPage, setItemsPerPage] = React.useState(optionsPerPage[0]);
  //const [itemsPerPage, setItemsPerPage] = React.useState(5);

  React.useEffect(() => {      
    setPage(0);
  }, [itemsPerPage]);

  return (
    <View>
      <DataTable>
      <DataTable.Header>
        <DataTable.Title style={{flex: 3,justifyContent:"center", backgroundColor: "white"}}>Product</DataTable.Title>
        <DataTable.Title style={{flex: 2,justifyContent:"center", backgroundColor: "white"}}>Qty</DataTable.Title>
        <DataTable.Title style={{flex: 2,justifyContent:"center", backgroundColor: "white"}}>UoM</DataTable.Title>
      </DataTable.Header>
{/* 
      <DataTable.Row>
        <DataTable.Cell>{data.key}</DataTable.Cell>
        <DataTable.Cell numeric>159</DataTable.Cell>
        <DataTable.Cell numeric>6.0</DataTable.Cell>
      </DataTable.Row> */}


        {data.map(d => {
            return (
            <DataTable.Row key={d.key}>
                <DataTable.Cell style={{flex: 3, backgroundColor: "white"}}>{d.product_name}</DataTable.Cell>    
                <DataTable.Cell numeric style={{flex: 2, backgroundColor: "white", paddingRight: 20}}>{d.item_qty_packing}</DataTable.Cell>
                <DataTable.Cell style={{flex: 2, justifyContent:"center",backgroundColor: "white"}}>{d.item_uom_packing}</DataTable.Cell>
            </DataTable.Row>
            );
        })}

      <DataTable.Pagination
        page={page}
        //numberOfPages={3}
        numberOfPages={Math.round (data.length / 3)}
        onPageChange={(page) => setPage(page)}
        label={page + 1}
        optionsPerPage={optionsPerPage}
        itemsPerPage={itemsPerPage}
        setItemsPerPage={setItemsPerPage}
        showFastPagination
        optionsLabel={'Rows per page'}
      />
    </DataTable>
    </View>
  )
}

export default DataTableComponents

const styles = StyleSheet.create({
    listItemContainer: {
        height: '40%',
        paddingHorizontal: 10

    }
})
import { StyleSheet, Text, View,  } from 'react-native'
import React from 'react'
import { Avatar, Button, Card, Title, Paragraph, TextInput, Switch, Subheading, Divider, Headline, Caption, useTheme   } from 'react-native-paper';
import { STATUS_DO_FAILED_SEND, STATUS_DO_INIT, STATUS_DO_SUCCES_SEND } from '../utils/Constants';
import { currencyFormat } from '../utils/CurrencyFormat';

const ItemInformationCard = ({
    product_id,
    product_name,
    qty_packing,
    uom_packing,
    qty_unit,
    uom_unit,
    qty_reject,
    uom_reject,
    sedang_cekin,
    already_sent,
    parentCallback
  }) => {

  const text4 = "QTY TOLAKAN (".concat(uom_packing, ')');
  const [qtyTolak, setQtyTolak] = React.useState("0");
  const [disabled, isDisabled] = React.useState(false);

  const [isSwitchOn, setIsSwitchOn] = React.useState(false);

  const onToggleSwitch = () => {
    setIsSwitchOn(!isSwitchOn)
    setQtyTolak("0");
    parentCallback({
      id: product_id,
      qty_tolak: parseInt(0)
    });
  }

  const onChangeTextMethod = (qtyTolak) => {
    //setQtyTolak(currencyFormat(qtyTolak));
    //setTextParkir(currencyFormat(textParkir));
    //console.log("masuk sini");

    if (parseInt(qtyTolak) > parseInt(qty_packing)){
      qtyTolak = "0";
      setQtyTolak(currencyFormat(qtyTolak));
    }

    if (parseInt(qtyTolak) < 0 ){
      qtyTolak = "0";
      setQtyTolak(currencyFormat(qtyTolak));
    }

    setQtyTolak(currencyFormat(qtyTolak));

    if (qtyTolak === ''){
      console.log('child component method 2');
      qtyTolak = "0";
      setQtyTolak(currencyFormat(qtyTolak));
      parentCallback({
        id: product_id,
        qty_tolak: parseInt(0)
      });
    } else {
    parentCallback({
      id: product_id,
      qty_tolak: parseInt(qtyTolak)
    });
    }

  }

  const onBlurMethod = (qtyTolak) => {
      /* console.log('child component method 1');
     
      setQtyTolak(parseInt(qtyTolak));
      if (qtyTolak > parseInt(qty_packing)){
        setQtyTolak(0)
      }

      if (qtyTolak < 0 ){
        setQtyTolak(0)
      }

      if (qtyTolak === ''){
        console.log('child component method 2');
        setQtyTolak(parseInt(0));
        parentCallback({
          id: product_id,
          qty_tolak: parseInt(0)
        });
      } else {
      parentCallback({
        id: product_id,
        qty_tolak: parseInt(qtyTolak)
      });
      } */

      if (parseInt(qtyTolak) > parseInt(qty_packing)){
        qtyTolak = "0";
        setQtyTolak(currencyFormat(qtyTolak));
      }
  
      if (parseInt(qtyTolak) < 0 ){
        qtyTolak = "0";
        setQtyTolak(currencyFormat(qtyTolak));
      }
  
      setQtyTolak(currencyFormat(qtyTolak));
  
      if (qtyTolak === ''){
        console.log('child component method 2');
        qtyTolak = "0";
        setQtyTolak(currencyFormat(qtyTolak));
        parentCallback({
          id: product_id,
          qty_tolak: parseInt(0)
        });
      } else {
      parentCallback({
        id: product_id,
        qty_tolak: parseInt(qtyTolak)
      });
      }
  }

  React.useEffect(() => {
    let isSubscribed = true;
    console.log('barang customer : ', already_sent)
    //console.log(barang_udah_kirim);

  //},[status_description]);
  },[]);

  return (
    <Card>
    <Card.Content style={styles.cardContainer}>
      <View style={styles.container}>
        <View style={[{ flex: 1, alignItems: "center"  } ]}>
          <Title>{product_id}</Title>
          <Paragraph>{product_name}</Paragraph>
        </View>
        <View style={[{ flex: 1, alignItems: "center" } ]}>
          <Title>Jumlah Kirim</Title> 
          <Paragraph>{qty_packing} {uom_packing} {qty_unit} {uom_unit}</Paragraph>
        </View>
      </View> 
      
      <>
      {
       sedang_cekin ?
      <View>
            <View style={[{ flex: 1, justifyContent: "center", alignContent: "center", alignItems: "center"  } ]}>
              
              {
                already_sent ?
                  <View>
                    <Subheading style={[{ color: useTheme().colors.error }]}>Jumlah Barang Tolakan : {qty_reject} {uom_reject}</Subheading>
                  </View>
                  :
                  <>
                  <Subheading style={[{ color: useTheme().colors.error }]}>Ada Barang Reject / Tolak ?</Subheading> 
                  <Switch value={isSwitchOn} onValueChange={onToggleSwitch} />
                  </>
              }
              
            </View>
            <>
            {
              isSwitchOn?
                <TextInput
                    label={text4}
                    keyboardType="numeric"
                    //onBlur={() => {onBlurMethod}}
                    //onChangeText={() => {onBlurMethod()}}
                    value={qtyTolak}
                    disabled={disabled}
                    //onChangeText={(qtyTolak) => setQtyTolak(qtyTolak)}
                    onChangeText={onChangeTextMethod}
                />
                :
                <View />
            }
            </>
       </View>
       :
       <View>
          <View style={[{ flex: 1, justifyContent: "center", alignContent: "center", alignItems: "center"  } ]}>
          {
                already_sent ?
                  <View>
                    <Subheading style={[{ color: useTheme().colors.error }]}>Jumlah Barang Tolakan : {qty_reject} {uom_reject}</Subheading>
                  </View>
                  :
                  <>
                  </>
          }
          </View>
       </View>
       }
       </>
    </Card.Content>
  </Card>
  )
}

export default ItemInformationCard

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
    },
    
})
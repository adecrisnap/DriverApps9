import { StyleSheet, View } from 'react-native'
import React from 'react'
import { Avatar, Card, IconButton, Text } from 'react-native-paper';

const ItemInformationCardWithDeleteIcon = ({id, title, qty, callback}) => {

  const removeItem = () => {
    callback(id)
  }

  return (
    <View>
       <Card.Title
        title={title} 
        subtitle={qty}
        left={(props) => <Avatar.Icon {...props} icon="package-variant" />}
        right={(props) => <IconButton {...props} icon="trash-can-outline" onPress={removeItem} />}
      />
    </View>
  )
}

export default ItemInformationCardWithDeleteIcon

const styles = StyleSheet.create({})
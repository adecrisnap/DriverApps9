import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Avatar, Card, TextInput } from 'react-native-paper'
import { currencyFormat } from '../utils/CurrencyFormat'

const ReturItemInformation = ({id, nama, qty, uom, parentCallback}) => {

  let text3 = nama + ' ' + qty + ' ' + uom
  let text4 = "Qty Retur ("
  let text5 = text4.concat(uom).concat(")");

  let qtyString =  String(qty);
  const [text, setText] = React.useState(qtyString);

  const onChangeTextMethod = (text) => {
    if (parseInt(text) <= parseInt(qty)){
      setText(currencyFormat(text));
    }
    if (parseInt(text) > parseInt(qty)){
      text = "0";
      setText(currencyFormat(text));
    }

    if (parseInt(text) < 0 ){
      text = "0";
      setText(currencyFormat(text));
    }

    parentCallback({
      id: id,
      qty_reject: parseInt(text)
    });

  }

  return (    
    <View>
        <Card.Title
            title={text3}
            left={(props) => <Avatar.Icon {...props} icon="package-variant" />}
        />
         <TextInput
            label={text5}
            value={text}
            keyboardType="numeric"
            onChangeText={onChangeTextMethod}
          />
    </View>
  )
}

export default ReturItemInformation

const styles = StyleSheet.create({})
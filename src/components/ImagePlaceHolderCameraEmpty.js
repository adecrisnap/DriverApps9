import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import React from "react";
import { Surface, Text, TouchableRipple, IconButton, Colors  } from "react-native-paper";
import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';

const ImagePlaceHolderCameraEmpty = ({navigation, route, image_id}) => {

    const [image, setImage] = React.useState(null);
     
    return (<Surface style={styles.surface}>
        <TouchableRipple rippleColor="rgba(0, 0, 0, .80)" >
           <IconButton
              icon="camera"
              color={Colors.red500}
              size={20}
              onPress={() => navigation.navigate("CameraPreviewScreen", {
                camera_preview_id: image_id
              })}
            /> 
        </TouchableRipple>
      </Surface>
        
    );
};

export default ImagePlaceHolderCameraEmpty;

const styles = StyleSheet.create({
  surface: {
    padding: 8,
    height: 120,
    width: 120,
    alignItems: "center",
    justifyContent: "center",
    elevation: 4,
    marginHorizontal: 10
  },
});

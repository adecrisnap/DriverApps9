import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Button } from 'react-native-paper'



const TransaksiTokoScreen = ({navigation, route}) => {

    React.useLayoutEffect(() => {
        navigation.setOptions({
          title: "Pilih Jenis Transaksi",
        });
      }, [navigation]);

  return (
    <View>
      <View>
        <Button icon="package-variant" mode="outlined" onPress={() => console.log('Pressed')}>
          Rekap Barang
        </Button>
        <Button icon="package-down" mode="outlined" onPress={() => console.log('Pressed')}>
          Ambil Barang Retur
        </Button>
        <Button icon="gift-outline" mode="outlined" onPress={() => console.log('Pressed')}>
          Input Barang Reject
        </Button>
        <Button icon="cash-plus" mode="outlined" onPress={() => console.log('Pressed')}>
          Input Petty Cash Per Toko
        </Button>
        </View>
    </View>
  )
}

export default TransaksiTokoScreen

const styles = StyleSheet.create({})
import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { BASE_URL, GET_PLANNER_HEADERID_DELIVERYTODAY_BY_DRIVERID, NGROK_URL } from "../utils/Constants";
import { selectUserId } from '../redux/slices/authSlice';
import { useDispatch, useSelector } from "react-redux";
import axios from 'axios';
import DashboardScreenData from './DashboardScreenData';
import { setLoadingFalse, setLoadingTrue } from '../redux/slices/loadingSlice';
import { useTheme } from 'react-native-paper';
import { PreferencesContext } from '../utils/PreferencesContext';

const DataNotExist = ({navigation, route}) => {
  return (
    <SafeAreaView>
    <Text>Belum ada Jadwal Hari ini</Text>
    </SafeAreaView>
  )
}

const DashboardScreen = ({ navigation, route }) => {
  const [datas, setDatas] = React.useState([]);
  const [dataEmpty, setDataEmpty] = React.useState([]);
  
  const uid = useSelector(selectUserId);
  const dispatch = useDispatch();
  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);

  React.useEffect(() => {
    //console.log("manggil useeffect tanpa param");
    let isSubscribed = true;
    dispatch(setLoadingTrue());
    const fetchData = async () => {      
      console.log("manggil query 1");
      console.log("uid", uid);
      axios
        .get(NGROK_URL + GET_PLANNER_HEADERID_DELIVERYTODAY_BY_DRIVERID + '/' + uid)
        .then(function (response) {
          console.log(response.data?.results.data);
          if (isSubscribed){
            setDatas(response.data?.results.data);
            dispatch(setLoadingFalse());
          }
        })
        .catch(function (error) {
          //console.log("ERROR CEK DELIVERY INPROGRESS : ", error);
          dispatch(setLoadingFalse());
        });
    }

    fetchData()
      .catch(console.error);
      //console.log("manggil query 2");
      console.log('Data ID Plan', datas.length);

      // const unsubscribe = navigation.addListener('focus', () => {
      //   setDatas = [];
      // });

     return () => isSubscribed = false;
    //  return () => {
    //   unsubscribe;
    //   };
  }, [])

  return (
    <>
    {
        // datas ? <DataExist data={datas} navigation={navigation}/> : <DataNotExist navigation={navigation}/>
        // datas ? <DashboardScreenData navigation={navigation} DATA={datas} route={route}/> : <DataNotExist navigation={navigation} route={route}/>

        datas && datas.length ? <DashboardScreenData navigation={navigation} arrayData={datas} route={route}/> : <DataNotExist navigation={navigation} route={route}/>

        //datas && datas.length ? <DashboardScreenData navigation={navigation} arrayData={datas} route={route}/> : <DashboardScreenData navigation={navigation} arrayData={dataEmpty} route={route}/>
    }
    </>
  )


}

export default DashboardScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})
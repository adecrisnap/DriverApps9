import { StyleSheet, View, Dimensions, Platform, FlatList, TouchableHighlight, KeyboardAvoidingView, Alert } from "react-native";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { useTheme, Text, Button, Caption, TextInput, TouchableRipple } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { selectUserId } from "../redux/slices/authSlice";
import { AutocompleteDropdown } from "react-native-autocomplete-dropdown";
import axios from "axios";
import { FAILED_SAVE_RN, GET_ALL_CUSTOMERS, GET_ALL_PRODUCTS, NGROK_URL, POST_RETUR_NON_SCHEDULED, showError, showSuccess, SUCCESS_SAVE_RN } from "../utils/Constants";
import AutoCompleteDropDownModif from "../components/AutoCompleteDropDownModif";
import { currencyFormat } from "../utils/CurrencyFormat";
import { getDataObjectNonSecured, removeKey, storeDataObjectNonSecured } from "../utils/LocalStorage";
import ItemInformationCardWithDeleteIcon from "../components/ItemInformationCardWithDelete";
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import NetInfo from "@react-native-community/netinfo";

const ReturnNonScheduled = ({ navigation, route }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const uid = useSelector(selectUserId);
  //const { customer_id, customer_name, deliveryplan_id } = route.params;

  const [qty, setQty] = React.useState("0");
  const [products, setProducts] = React.useState([]);


  const onChangeTextQty = (qty) => {
    if (qty === "") {
      qty = "0";
    }
    setQty(currencyFormat(qty));
  };

  const handleCallBackCustomer = async (customer) => {
    //console.log('Customer method', customer);   

    await storeDataObjectNonSecured("customer", customer);

  }

  const handleCallBackProduct = async (product) => {
    //console.log('Product method', item);   
    await removeKey("product");
    await storeDataObjectNonSecured("product", product);
  }

  const callBackRemoveItemFromList = async (id) => {
    let newData = products.filter(o => o.id_product !== id) ;
    setProducts(newData);
  }


  const saveProductToList = async () => {
    const product = await getDataObjectNonSecured("product");

    const newProduct = {
      id_product: product.id,
      nama_product: product.title,
      qty_product:qty,
    };

    if ( Number.parseFloat(qty) <= 0 ){
      return;
    }

    
    if (products.length == 0) {
        setProducts([...products, newProduct]);
    } else {
      //console.log("masuk else")
      let obj = products.find(o => o.id_product === newProduct.id_product) ? true : false;
      //console.log(obj)
      if (!obj) {
        setProducts([...products, newProduct]);
      }
    }
  }

  const saveReturNonScheduled = async () => {
    dispatch(setLoadingTrue());

    NetInfo.fetch().then(async state => {
      if (state.isConnected == true) {

        const date_iso = new Date().toISOString();
        let newCustomer = await getDataObjectNonSecured("customer");

        const response = await axios.post(NGROK_URL + POST_RETUR_NON_SCHEDULED, {
          
          data: {
            driverid: uid,
            customer_id: newCustomer.id,
            customer_name: newCustomer.title,
            date_isoformat: date_iso,
            produk: products,
          }

        })

        if (response.status == 200) {
          dispatch(setLoadingFalse());
          showSuccess(SUCCESS_SAVE_RN);
          navigation.navigate("Dashboard"); 
          return;
        } else {
          dispatch(setLoadingFalse());
          showError(FAILED_SAVE_RN);
          return;
        }
      } else {
          dispatch(setLoadingFalse());
          Alert.alert(FAILED_NETWORK);
       }
    });
  }

  /* const addToListProduct = () => {

    const newProduct = {
      id_product: selectedItem,
      nama_product: "",
      qty_product:qty,
    };
    
    setProducts([...products, newProduct]);
  } */


  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "Input Transaksi Retur",
    });
  }, [navigation]);

  return (
    <SafeAreaView style={[{ backgroundColor: theme?.colors.surface, flex: 1 }]}>
      
      <View style={styles.container}>
        <View style={styles.upperContainer}>
          <View>
          <Caption>Cari Customer</Caption>
          <AutoCompleteDropDownModif zIndex={100} url_locator={NGROK_URL + GET_ALL_CUSTOMERS} filter_id={"id"} filter_by={"value"} parentCallback = {handleCallBackCustomer}  /> 
          </View>

          <Caption>Cari Nama Produk</Caption>
          <AutoCompleteDropDownModif zIndex={90} url_locator={NGROK_URL + GET_ALL_PRODUCTS} filter_id={"product_code"} filter_by={"product_name"} parentCallback = {handleCallBackProduct}  />

          <Caption>Input Qty Retur</Caption>
          <TextInput
            label="Qty"
            value={qty}
            mode="outlined"
            onChangeText={onChangeTextQty}
            keyboardType="numeric"
          />
         
          <Caption></Caption>
          <Button 
            mode="contained"
            onPress={saveProductToList} >
              Tambah Produk
          </Button>
          <View>
            <FlatList
              data={products}
              keyExtractor={item => item.id_product}
              renderItem={({ item, index }) => (
                <ItemInformationCardWithDeleteIcon
                  id={item.id_product}
                  title={item.nama_product}
                  qty={item.qty_product}
                  callback={callBackRemoveItemFromList}
                />
              )}              
            />
          </View>
        </View>
      
        </View>

        <View style={styles.bottomContainer}>
          <Button
            onPress={saveReturNonScheduled} 
            mode="contained" >
              Simpan Data Retur
          </Button>
        </View>
     
      
    </SafeAreaView>
  );
};

export default ReturnNonScheduled;

const styles = StyleSheet.create({
    container: {
      marginHorizontal: 20,
      flexDirection: "column",
      flex: 1,
      //justifyContent: 'space-between' 
    },
    upperContainer: {
      flex: 20
    },
    bottomContainer: {
      flex: 1,
      marginHorizontal: 20,
      marginBottom: 10,
      justifyContent: 'flex-end' 
    }
   
});

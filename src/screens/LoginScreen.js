import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { StyleSheet, View } from "react-native";
import { TextInput, Button, Avatar, TouchableRipple, Switch, useTheme  } from "react-native-paper";
import { useDispatch } from "react-redux";
import { setSignIn, setSignOut } from "../redux/slices/authSlice";
import { setLoading, setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import { BASE_URL, INVALID_LOGIN, LOCATION_START_TASK_NAME, NO_USERNAME_PASSWORD_INPUT, showError, URI_APP_LOGIN } from "../utils/Constants";
import axios from "axios";
import { PreferencesContext } from '../utils/PreferencesContext';
import { getDataObjectNonSecured, removeAllKey, storeDataObjectNonSecured, storeDataStringNonSecured } from "../utils/LocalStorage";
import { storeDataStringSecured, getDataStringSecured, deleteDataStringSecured } from "../utils/SecureLocalStorage"
import AsyncStorage from "@react-native-async-storage/async-storage";

import { axiosInstance } from "../config/axiosConfig";

const LoginScreen = () => {
  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);

  const [isSwitchOn, setIsSwitchOn] = React.useState(false);
  const onToggleSwitch = () => {
    setIsSwitchOn(!isSwitchOn);
    toggleTheme();
  }

  const dispatch = useDispatch();

  const [nik, setNIK] = React.useState("");
  const [pass, setPass] = React.useState("");
  const [isSecuredTextEntry, setSecuredTextEntry] = React.useState(true);

  const handleLogin2 = async() => {

    //dispatch(setLoading(true));
    dispatch(setLoadingTrue()); //disini langsung nongol nih loading klo gw panggil dispatch setloadingtrue

    if (nik === 'undefined' || nik === '' ) {
      showError(NO_USERNAME_PASSWORD_INPUT)
      //dispatch(setLoading(false));
      dispatch(setLoadingFalse());
      dispatch(setSignOut());
      return;
    }

    if (pass === 'undefined' || pass === '' || pass == undefined) {
      showError(NO_USERNAME_PASSWORD_INPUT)
      //dispatch(setLoading(false));
      dispatch(setLoadingFalse());
      dispatch(setSignOut());
      return;
    }
    
    await axios.post(BASE_URL + URI_APP_LOGIN, {
    //  await axiosInstance.post(BASE_URL + URI_APP_LOGIN, {
          nik: nik,
          appid: "6",
          pwd: pass,
          nonkmdb: "1",
          isencrypted: "false",
    })
    .then(function (response) {
      dispatch(setLoadingFalse());

      const user = {
        isLoggedIn: true,
        userName: response.data.partner_name,
        userId: response.data.id,
        userToken: response.data.token,
      }
      storeDataStringSecured('X-TOKEN',user.userToken );
      dispatch(setSignIn(user));
      
    })
    .catch(function (error) {
      console.log(error);
      showError(INVALID_LOGIN)
      dispatch(setLoadingFalse());
    });
  };

  const handleLogin = async() => {

    //dispatch(setLoading(true));
    //dispatch(setLoadingTrue());

    if (nik === 'undefined' || nik === '' ) {
      showError(NO_USERNAME_PASSWORD_INPUT)
      //dispatch(setLoading(false));
      dispatch(setLoadingFalse());
      dispatch(setSignOut());
      return;
    }

    if (pass === 'undefined' || pass === '' || pass == undefined) {
      showError(NO_USERNAME_PASSWORD_INPUT)
      //dispatch(setLoading(false));
      dispatch(setLoadingFalse());
      dispatch(setSignOut());
      return;
    }
    
    try {
      dispatch(setLoadingTrue()); // ini manggil nya, dia bakal kena di route gw
      const response = await axios.post(BASE_URL + URI_APP_LOGIN, {
            nik: nik,
            appid: "6",
            pwd: pass,
            nonkmdb: "1",
            isencrypted: "false",
      })

      //console.log(response);
      //console.log(response.data);
      //console.log(response.status);
      if (response?.status == 200){
          const user = {
            isLoggedIn: true,
            userName: response.data.partner_name,
            userId: response.data.id,
            userToken: response.data.token,
            loggin_date: new Date().toISOString(),
          }

          //console.log(user);
          storeDataStringSecured('X-TOKEN',user.userToken );

          await AsyncStorage.setItem('@MyApp_user', JSON.stringify(user))
          const currentUser = await AsyncStorage.getItem('@MyApp_user')
          //console.log('CURRENT USER ', currentUser)

          dispatch(setSignIn(user));
          dispatch(setLoadingFalse());

          // await ExpoLocation.startLocationUpdatesAsync(LOCATION_START_TASK_NAME, {
          //   accuracy: ExpoLocation.Accuracy.Highest,
          // });

      } else {

      }
    } catch (e) {
      //console.log(e);
      dispatch(setLoadingFalse());
      dispatch(setSignOut());
      showError(INVALID_LOGIN);
    }
  };

  const handleSecurePassword = () => {
    //console.log("Password");
    if (isSecuredTextEntry) {
      setSecuredTextEntry(false);
    } else {
      setSecuredTextEntry(true);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
     
      <View>
        <View style={styles.imageContainer}>
          <Avatar.Image
            size={144}
            source={require("../../assets/delivery-mask.jpg")}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            label="Username"
            mode="outlined"
            right={<TextInput.Icon name="text" />}
            value={nik}
            onChangeText={(nik) => setNIK(nik)}
          />

          <TextInput
            label="Password"
            secureTextEntry={isSecuredTextEntry}
            mode="outlined"
            right={
              isSecuredTextEntry ? (
                <TextInput.Icon name="eye-off" onPress={handleSecurePassword} />
              ) : (
                <TextInput.Icon name="eye" onPress={handleSecurePassword} />
              )
            }
            value={pass}
            onChangeText={(pass) => setPass(pass)}
          />
        </View>

        <View style={styles.buttonContainer}>
          <Button mode="contained" onPress={handleLogin}>
            Log In
          </Button>
        </View>

        <TouchableRipple>
        <Switch
          color={'red'}
          value={isThemeDark}
          onValueChange={onToggleSwitch}
        />
        </TouchableRipple>
      
      </View>
      
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  imageContainer: {
    alignItems: "center",
    marginBottom: 64,
    marginTop: 16,
  },
  inputContainer: {
    marginBottom: 32,
  },
  buttonContainer: {},
});

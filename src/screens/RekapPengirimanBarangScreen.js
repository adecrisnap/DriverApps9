import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  StatusBar,
  SafeAreaView,
  Image,
  Alert
} from "react-native";
import React from "react";
import {
  Button,
  Caption,
  Divider,
  List,
  Paragraph,
  Title,
  useTheme,
} from "react-native-paper";
import { Subheading, Headline } from "react-native-paper";
import { PreferencesContext } from "../utils/PreferencesContext";
import {
  FAILED_IMAGE_UPLOAD,
  FAILED_NETWORK,
  FAILED_SAVE_GR,
  GET_PLANNER_ACCUMULATED_SO_ITEM,
  GET_PLANNER_SO_NUMBER_BY_DELIVERYPLAN_CUSTOMERID,
  NGROK_URL,
  NO_DATA_FOUND,
  PICTURE_NOT_AVAILABLE,
  POST_GOODS_RECEIVE,
  POST_IMAGE,
  showError,
  showSuccess,
  SUCCESS_SAVE_GR,
} from "../utils/Constants";
import ItemInformationCard from "../components/ItemInformationCard";
import Gap from "../components/Gap";
import ImagePlaceHolderCameraEmpty from "../components/ImagePlaceHolderCameraEmpty";
import NetInfo from "@react-native-community/netinfo";
import axios from "axios";
import { nanoid } from 'nanoid';
import { getDataObjectNonSecured, getDataStringNonSecured, getFilterAsyncStorage, removeKey, storeDataObjectNonSecured, storeDataStringNonSecured } from "../utils/LocalStorage";
import { manipulateAsync, FlipType, SaveFormat } from 'expo-image-manipulator';
import { useDispatch, useSelector } from "react-redux";
import { selectUserId } from "../redux/slices/authSlice";
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";


const RekapPengirimanBarangScreen = ({ navigation, route }) => {
  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);
  const [items, setItems] = React.useState([]);
  const [soNumbers, setSONumbers] = React.useState([]);
  const uid = useSelector(selectUserId);
  const [alreadySent, setAlreadySent] = React.useState(false);
  let barangUdahKirim = false;

  //const [tolakan, setItemTolakan] = React.useState();

  const { rute_id, customer_id, sedang_cekin, status_description } = route.params;
  //const [imagePathPublic, setImagePathPublic] = React.useState('');
  const dispatch = useDispatch();

  const statusSent = async() => {
    console.log("status_description", status_description);

    if(status_description === "Sudah Dikirim"){
      setAlreadySent(true);
    }

    if(status_description === "Gagal Kirim"){
      setAlreadySent(true);
    }

    if(status_description === "Belum Dikirim"){
      setAlreadySent(false);
    }
    /* let t = await getDataObjectNonSecured(customer_id + "-KIRIM" );

    if (t == null){
      console.log('data t null')
      await storeDataObjectNonSecured(customer_id + "-KIRIM" );
    }

    let z = await getDataObjectNonSecured(customer_id + "-KIRIM" );
    console.log('data x',z);

    if (z != null){
      console.log("data null")
      setAlreadySent(true);
    } else {
      setAlreadySent(false);
    } */
  }

  let eachToko = {
    id: '',
    kiriman: [],
    tolakan: [],
  };  


  const handleCallBackItemInformationTolakanData = async (itemTolakanData) => {
    console.log('Parent method', itemTolakanData);
    //setItemTolakan(itemTolakanData);
    if (getDataObjectNonSecured("TOLAK-" + itemTolakanData.id) != null) {
      await removeKey("TOLAK-" + itemTolakanData.id);
    }

    await storeDataObjectNonSecured("TOLAK-" + itemTolakanData.id, itemTolakanData);
    //console.log('Cek Storage', getDataObjectNonSecured("TOLAK-" + itemTolakanData.id));
  }


  const uploadImageAsync = async (inputParam) => {
    let param = inputParam;
    let headers = {
       'Content-Type': 'multipart/form-data',
       Accept: 'application/json',
    };
    // let obj = {
    //   method: 'POST',
    //   headers: headers,
    //   body: param,
    // };

    //return axios.post(NGROK_URL  + POST_IMAGE, obj)
    /* return axios.post(NGROK_URL + POST_IMAGE, param, {
      headers: headers,
      })
      .then( function (response) {
        //let json = null;
        //json = resp.json();
        //console.log(' Response', response);
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
        throw error;
      }) */

      try {
        const response = await axios.post(NGROK_URL + POST_IMAGE, param, {
        headers: headers,
        })

      return response;
    } catch (e){
      console.log('error ' , e);
      throw e;
    }
  }

  const apiPostWithTokenAndImage = async (inputParam, apiName) => {
   
     let URL = WebAPI.BaseURL + apiName;
     console.log('URL:' + URL);
     let param = inputParam;
     let headers = {
       'Content-Type': 'multipart/form-data',// this is a imp line
       Accept: 'application/json',
     };
     let obj = {
       method: 'POST',
       headers: headers,
       body: param,
     };
     return fetch(URL, obj)// put your API URL here
       .then(resp => {
         let json = null;
         json = resp.json();
         console.log(apiName + ' Response', json);
         if (resp.ok) {
           return json;
         }
         return json.then(err => {
           console.log('error :', err);
           throw err;
         });
       })
       .then(json => json);
   };

   const failedKirimBarangGR = async () => {
    dispatch(setLoadingTrue());

    NetInfo.fetch().then(async state => {
      if (state.isConnected == true) {
        const formData = new FormData();
        const imageNameId = nanoid(15);
        const thisMonth = new Date().getMonth() + 1;
        const thisMonthString = thisMonth.toString();
        const thisYear = new Date().getFullYear();
        const thisYearString = thisYear.toString();

        const date_iso = new Date().toISOString();

        let newArrReject = JSON.parse(JSON.stringify(items));
        newArrReject["id"]="";
        newArrReject["qty_tolak"]="0";

        console.log ("Array Reject", newArrReject);
        
        newArrReject.forEach(arr => {
            arr["qty_reject"] = arr["qty_packing"];
            arr["uom_reject"] = arr["uom_packing"];  
            arr["id"] = arr["product_id"];
            arr["qty_tolak"] =  arr["qty_packing"];
        })

        console.log ("Array Reject 2", newArrReject);
        
       const response = await axios.post(NGROK_URL + POST_GOODS_RECEIVE, {

          data: {
              driverid: uid,
              custid: customer_id,
              ruteid: rute_id,
              imagePath: "",
              sonumbers: soNumbers,
              goodssent: items,
              goodsreject: newArrReject,
              date_isoformat: date_iso,
              reason: "FAILED TO DELIVER",
            }})

            const data1 = {
              driverid: uid,
              custid: customer_id,
              ruteid: rute_id,
              imagePath: "",
              sonumbers: soNumbers,
              goodssent: items,
              goodsreject: newArrReject,
            }

            storeDataObjectNonSecured(customer_id + "-KIRIM", data1 );

          if (response.status == 200) {
            
            //console.log(response.data);
            dispatch(setLoadingFalse());
            showSuccess(SUCCESS_SAVE_GR);
            
            navigation.navigate("Dashboard"); 
            return;
          } else {
            dispatch(setLoadingFalse());
            showError(FAILED_SAVE_GR);
            removeKey(customer_id + "-KIRIM");
            //delet
            return;
          } 
         
        
        
      } else {
          dispatch(setLoadingFalse());
          Alert.alert(FAILED_NETWORK);
       }
    });
   }

  const postDataGR = async() =>
  {

    dispatch(setLoadingTrue());
    //console.log('pressed')
    if (route.params?.imageUri == undefined) {
      showError(PICTURE_NOT_AVAILABLE);
      dispatch(setLoadingFalse());
      return;
    }


    NetInfo.fetch().then(async state => {
      if (state.isConnected == true) {
        const formData = new FormData();
        const imageNameId = nanoid(15);
        const thisMonth = new Date().getMonth() + 1;
        const thisMonthString = thisMonth.toString();
        const thisYear = new Date().getFullYear();
        const thisYearString = thisYear.toString();

        const date_iso = new Date().toISOString();

        const resizePhoto = await manipulateAsync(
          route.params?.imageUri,
          //[{ resize: { width: 500, height: 550 } }],
          [{ resize: { width: 480 } }],
          { compress: 0, format: "png", base64: false }
        );

        //console.log(resizePhoto);

        formData.append("image", {
          //uri: route.params?.imageUri,
          uri: resizePhoto?.uri,
          type: "image/jpg",
          name: 'IMG' + thisMonthString + thisYearString + imageNameId + ".jpg",
        });

        let resultFotoUpload = await uploadImageAsync(
          formData
        );

        if (resultFotoUpload.status == 200) {
          
          const imagePath = resultFotoUpload.data;
          const imagePathString =  imagePath[0].path
          const dataTolakan = await getFilterAsyncStorage('TOLAK');
        
          const response = await axios.post(NGROK_URL + POST_GOODS_RECEIVE, {

          data: {
              driverid: uid,
              custid: customer_id,
              ruteid: rute_id,
              imagePath: imagePathString,
              sonumbers: soNumbers,
              goodssent: items,
              goodsreject: dataTolakan,
              date_isoformat: date_iso,
              reason: "REJECT BY CUSTOMER IF ANY",
            }})

            const data1 = {
              driverid: uid,
              custid: customer_id,
              ruteid: rute_id,
              imagePath: imagePathString,
              sonumbers: soNumbers,
              goodssent: items,
              goodsreject: dataTolakan,
            }

            storeDataObjectNonSecured(customer_id + "-KIRIM", data1 );

          if (response.status == 200) {
            
            //console.log(response.data);
            dispatch(setLoadingFalse());
            showSuccess(SUCCESS_SAVE_GR);
            
            navigation.navigate("Dashboard"); 
            return;
          } else {
            dispatch(setLoadingFalse());
            showError(FAILED_SAVE_GR);
            removeKey(customer_id + "-KIRIM");
            //delet
            return;
          }
        }
        else {
             dispatch(setLoadingFalse());
            showError(FAILED_IMAGE_UPLOAD);
            return;
        }
        

        // formData.append('first_name', userName);
        // formData.append('last_name', lastName);
        // formData.append('phone_number', phone_number);
        // formData.append('email', email);
        // formData.append('address', address);
        // formData.append('image', {
        //     uri: imageURI,
        //     type: "image/jpeg",
        //     name: "photo.jpg"
        //  }) 
        // let res = await apiPostWithTokenAndImage(
        //   formData,
        //   WebAPIURL,
        // );
        // this.setState(async function() {
        //   if (res.status == OK) {
        //    Alert.alert('Success');
        //   } 
        //   else {
        //     Alert.alert('Fail');
        //   }
        // });
      } else {
          dispatch(setLoadingFalse());
          //Alert.alert('Network Issue');
          Alert.alert(FAILED_NETWORK);
       }
    });
  }

  React.useLayoutEffect(() => {
    //console.log({ sedang_cekin });

    let isSubscribed = true;

    navigation.setOptions({
      title: "Data Rekap Pengiriman Barang",
    });

    const fetchData = async (rute_id, customer_id) => {
      await axios
        .get(
          NGROK_URL +
            GET_PLANNER_ACCUMULATED_SO_ITEM +
            "/" +
            rute_id +
            "/" +
            customer_id
        )
        .then(function (response) {
          //console.log(response.data.results.data)
          if (isSubscribed) {
            setItems(response.data.results.data);
          }
        })
        .catch(function (error) {
        });

      await axios
        .get(
          NGROK_URL +
            GET_PLANNER_SO_NUMBER_BY_DELIVERYPLAN_CUSTOMERID +
            "/" +
            rute_id +
            "/" +
            customer_id
        )
        .then(function (response) {
          console.log('data SO  : ', response.data.results.data)
          if (isSubscribed) {
            setSONumbers(response.data.results.data);
          }
        })
        .catch(function (error) {
        });
    };

    //console.log('Rekap Cust ID', customer_id)
    //console.log('Rekap Rute ID', rute_id)

    fetchData(rute_id, customer_id);

    //console.log('item ', items)
    return () => (isSubscribed = false);
  }, [navigation]);

  React.useEffect(() => {
    statusSent();
   
    eachToko.id = customer_id;
    
    items.map((item) => {
      eachToko.kiriman.push({
        "product_id": item.product_id,
        "product_name": item.product_name,
        "qty_packing": item.qty_packing,
        "uom_packing": item.uom_packing,
        "qty_unit": item.qty_unit,
        "uom_unit": item.uom_unit,
      });
    })

  }, []);

  React.useEffect(() => {
    //console.log('apa udh kirim? ', alreadySent);
    if (alreadySent){
      barangUdahKirim = true;
    } else {
      barangUdahKirim = false;
    }

    //console.log('barang udh kirim? ', barangUdahKirim);
  },[alreadySent]);

  return (
    <SafeAreaView style={[{ backgroundColor: theme?.colors.surface, flex: 1 }]}>
      <>
        {items && items.length ? (
          <>
            {
               sedang_cekin && !alreadySent ?
              //!alreadySent ?
            <View
              style={[
                {
                  backgroundColor: "theme?.colors.surface",
                  flex: 2,
                  alignItems: "center",
                },
              ]}
            >
              <Subheading>Upload Foto Pendukung</Subheading>
              <View>
                {route.params?.placeholder_id === "1" ? (
                  <Image
                    source={{ uri: route.params?.imageUri }}
                    style={{ width: 120, height: 120 }}
                  />
                ) : (
                  <ImagePlaceHolderCameraEmpty
                    navigation={navigation}
                    route={route}
                    image_id="1"
                  />
                )}
              </View>
            </View> :
            <View />
            }
            <View
              style={[{ backgroundColor: "theme?.colors.surface", flex: 5 }]}
            >
              {/* <Subheading style={[{ textAlign: "center" }]}>
                Data Kiriman Barang
              </Subheading> */}
              <FlatList
                data={items}
                keyExtractor={(item, index) => String(item.product_id)}
                renderItem={({ item, index }) => (
                  <ItemInformationCard
                    product_id={item.product_id}
                    product_name={item.product_name}
                    qty_packing={item.qty_packing}
                    uom_packing={item.uom_packing}
                    qty_unit={item.qty_unit}
                    uom_unit={item.uom_unit}
                    qty_reject={item.qty_reject}
                    uom_reject={item.uom_reject}
                    sedang_cekin={sedang_cekin}
                    already_sent={alreadySent}
                    parentCallback = {handleCallBackItemInformationTolakanData}
                  />
                )}
              />
            </View>
            
            {
              sedang_cekin && !alreadySent ?
              //!alreadySent ?
              <> 
            <Divider />
            <View
              style={[{ backgroundColor: "theme?.colors.surface", flex: 1 }]}
            >
              <Button
                mode="outlined"
                icon="content-save-outline"
                labelStyle={[{ textTransform: "capitalize" }]}
                onPress={postDataGR}
              >
                Simpan Data Kirim Barang
              </Button>
              <Gap height={12} />
              <Button
                mode="contained"
                icon="cancel"
                labelStyle={[{ textTransform: "capitalize" }]}
                onPress={failedKirimBarangGR}
              >
                Gagal Kirim Barang
              </Button>
            </View>
            </>
            :
            <View>

            </View>
            }
          </>
        ) : (
          <View style={styles.noData}>
            <Title>{NO_DATA_FOUND}</Title>
          </View>
        )}
      </>
    </SafeAreaView>
  );
};

export default RekapPengirimanBarangScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  noData: {
    flex: 1,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
  },
});

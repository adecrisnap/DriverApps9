import { Alert, FlatList, StyleSheet, Text, View } from "react-native";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { Button, Title, useTheme } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { selectUserId } from "../redux/slices/authSlice";
import {
  FAILED_NETWORK,
  FAILED_SAVE_RN,
  GET_RETURNSCHEDULEDID_NOT_PROCESSED,
  GET_RETURNSCHEDULED_NOT_PROCESSED,
  NGROK_URL,
  NO_DATA_FOUND,
  POST_RETUR_NON_SCHEDULED,
  POST_RETUR_SCHEDULED,
  showError,
  showSuccess,
  SUCCESS_SAVE_RN,
} from "../utils/Constants";
import ReturItemInformation from "../components/ReturItemInformation";
import { axiosInstance } from "../config/axiosConfig";
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";

import NetInfo from "@react-native-community/netinfo";
import axios from "axios";
import { getDataObjectNonSecured, getFilterAsyncStorage, removeKey, storeDataObjectNonSecured } from "../utils/LocalStorage";

const ReturScreen = ({ navigation, route }) => {
  const { customer_id } = route.params;
  const theme = useTheme();
  const dispatch = useDispatch();
  const uid = useSelector(selectUserId);

  const [rejects, setRejects] = React.useState([]);
  const [rejectsID, setRejectsID] = React.useState([]);

  const handleCallBack = async (itemReject) => {
    console.log("Parent method", itemReject);
    if (getDataObjectNonSecured("REJECT-" + itemReject.id) != null) {
      await removeKey("REJECT-" + itemReject.id);
    }

    await storeDataObjectNonSecured(
      "REJECT-" + itemReject.id,
      itemReject
    );
  };

  const simpanDataReturScheduled = async () => {

    // Alert.alert("sedang dalam pengembangan");
    // return;

    dispatch(setLoadingTrue());

    NetInfo.fetch().then(async (state) => {
      if (state.isConnected == true) {
        const date_iso = new Date().toISOString();
        //console.log(date_iso);
        const dataReject = await getFilterAsyncStorage('REJECT');
        const response = await axios.post(NGROK_URL + POST_RETUR_SCHEDULED, {
          data: {
            user_id: uid,
            customer_id: customer_id,
            date_isoformat: date_iso,
            reject: dataReject,
            rejectID: rejectsID,
          },
        });

        if (response.status == 200) {
          showSuccess(SUCCESS_SAVE_RN);
          console.log(response.data);
          dispatch(setLoadingFalse());
          navigation.navigate("Dashboard");
          return;
        } else {
          console.log(response.status);
          showError(FAILED_SAVE_RN);
          dispatch(setLoadingFalse());
          return;
        }
      } else {
        dispatch(setLoadingFalse());
        Alert.alert(FAILED_NETWORK);
      }
    });

    dispatch(setLoadingFalse());
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "Ambil Barang Retur",
    });
  }, [navigation]);

  React.useEffect(() => {
    const fetchData = async () => {
      await axiosInstance
        .get(NGROK_URL + GET_RETURNSCHEDULED_NOT_PROCESSED + "/" + customer_id)
        .then(function (response) {
          console.log(response.data.results.data);
          setRejects(response.data.results.data);
        })
        .catch(function (error) {});
    };

    const fetchDataID = async () => {
      await axiosInstance
        .get(
          NGROK_URL + GET_RETURNSCHEDULEDID_NOT_PROCESSED + "/" + customer_id
        )
        .then(function (response) {
          console.log(response.data.results.data);
          setRejectsID(response.data.results.data);
        })
        .catch(function (error) {});
    };

    fetchData();
    fetchDataID();
  }, []);

  return (
    <SafeAreaView style={[{ backgroundColor: theme?.colors.surface, flex: 1 }]}>
      <>
        {rejects && rejects.length ? (
          <>
          <View>
            <FlatList
              data={rejects}
              keyExtractor={(item, index) => String(index)}
              renderItem={({ item, index }) => (
                <ReturItemInformation
                  id={item.Returnd_productid}
                  nama={item.Art_name}
                  qty={item.Returnd_qty}
                  uom={item.Art_einheit3}
                  parentCallback={handleCallBack}
                />
              )}
            />
          </View>

          <View style={styles.bottomContainer}>
            <Button onPress={simpanDataReturScheduled} mode="contained">
              Simpan Data Retur
            </Button>
          </View>
          </>
       ) : (
        <View style={styles.noData}>
          <Title>{NO_DATA_FOUND}</Title>
        </View>
      )}
      </>
    </SafeAreaView>
  );
};

export default ReturScreen;

const styles = StyleSheet.create({});

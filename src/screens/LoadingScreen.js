import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { Colors } from 'react-native-paper';
import React from 'react'

const LoadingScreen = () => {
  return (
    <View style={styles.wrapper}>
          <ActivityIndicator size="large" color={Colors.red800}  /> 
        <Text style={styles.text}>Loading...</Text>
    </View>
  )
}

export default LoadingScreen

const styles = StyleSheet.create({
    wrapper: {
      flex: 1,
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
      width: '100%',
      height: '100%',
    },
    text: {
      fontSize: 20,
      color: "white",
      marginTop: 16,
    },
  });
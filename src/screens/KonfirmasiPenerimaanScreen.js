import { StyleSheet, View } from "react-native";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import axios from "axios";
import {
  Divider,
  Text,
  Subheading,
  Button,
  Switch,
  Title,
  useTheme,
} from "react-native-paper";
import { DataTable } from "react-native-paper";

import {
  NGROK_URL,
  GET_DATAITEM_BY_DO,
  POST_IMAGE,
  POST_GOODS_RECEIVE_DATA,
  POST_DATA_GOODS_RECEIVE_FAILED,
  showError,
  PICTURE_NOT_AVAILABLE,
  RETUR_DATA_NOT_EXISTS,
} from "../utils/Constants";
import ImagePlaceHolderCameraEmpty from "../components/ImagePlaceHolderCameraEmpty";
import DataTableComponents from "../components/DataTableComponents";
import { PreferencesContext } from "../utils/PreferencesContext";
import { selectUserId } from "../redux/slices/authSlice";
import { useDispatch, useSelector } from "react-redux";
import 'react-native-get-random-values';
import { nanoid } from 'nanoid';
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import { selectIsReceiveAll, setReceiveAllFalse, setReceiveAllTrue } from "../redux/slices/receiveSlice";

const KonfirmasiPenerimaanScreen = ({ navigation, route }) => {
  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);
  const uid = useSelector(selectUserId);
  //const [ imagePath, setImagePath ] = React.useState(null); 

  //const optionsPerPage = [2, 3, 4];
  const {
    customer_id,
    customer_name,
    deliveryplan_id,
    do_numer,
    dn_number,
    status_do,
    isAlreadyCheckInOnPlace,
  } = route.params;

  const [title, onChangeTitle] = React.useState(route.params.title);
  const [originalArrayItem, setOriginalArrayItem] = React.useState([]);
  const [receiveArrayItem, setReceiveArrayItem] = React.useState([]);
  const [returArrayItem, setReturArrayItem] = React.useState([]);
  const [isSwitchAllGoodsReceiveOn, setIsSwitchAllGoodsReceiveOn] =
    React.useState(true);
  //const [page, setPage] = React.useState(0);
 
  const dispatch = useDispatch();

  const recvAllState = useSelector(selectIsReceiveAll);
  
  const onToggleAllGoodReceiveSwitch = () =>
  {
    setIsSwitchAllGoodsReceiveOn(!isSwitchAllGoodsReceiveOn);
  }

  const fetchDataItemByDO = React.useCallback(async () => {
    axios
      .get(NGROK_URL + GET_DATAITEM_BY_DO + "/" + do_numer)
      .then(function (response) {
        setOriginalArrayItem(response.data);
      })
      .catch(function (error) {
        //console.log("ERROR : ", error);
      });
  }, []);

  const postDataAndImageGoodsReceive = async () => {
    if (!recvAllState && returArrayItem.length == 0) {
      showError(RETUR_DATA_NOT_EXISTS);
      return;
    }

    //console.log("URI : ", route.params?.imageUri);
    if (route.params?.imageUri == undefined) {
      //console.log("masuk sini URI");
      showError(PICTURE_NOT_AVAILABLE);
      return;
    } else {
      const data = new FormData();
      const imageNameId = nanoid(15);
      var thisMonth = new Date().getMonth() + 1;
      var thisMonthString = thisMonth.toString();
      var thisYear = new Date().getFullYear();
      var thisYearString = thisYear.toString();
      
      data.append("image", {
        uri: route.params?.imageUri,
        type: "image/jpg",
        name: 'IMG' + thisMonthString + thisYearString + imageNameId + ".jpg",
      });

      const headers = {
        "Content-Type": "multipart/form-data",
        Accept: "application/json",
      };

      /* const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
        },
        body: data,
      }; */

      //console.log ('imageData ', data)

      /* await axios.post(NGROK_URL + POST_IMAGE, data, {
        headers: headers,
      })
      .then(function (response) {
        setImagePath(response.data);
      }) */
      
      try {
        const response = await axios.post(NGROK_URL + POST_IMAGE, data, {
        headers: headers,
        })

        
      console.log(response.data);
      const imagePath = response.data;

      console.log('image path ', imagePath);
      console.log('image path 2', imagePath[0].path);
      
    } catch (e){
      console.log('error ' , e);
    }

      /* if (recvAllState)
      {
        console.log('Semua GR direceive');
        const response = await axios.post(NGROK_URL + POST_GOODS_RECEIVE_DATA, {
          data: {
            driverid: uid,
            custid: customer_id,
            custname: customer_name,
            deliveryplanid: deliveryplan_id,
            donumber: do_numer,
            dnnumber: dn_number,
            imagePath: imagePath,
            isReceiveAllItem: isSwitchAllGoodsReceiveOn,
            originalArray: originalArrayItem,
            receiveArray: originalArrayItem,
            returArray: [],
          },
        })

        console.log('response status ', response.status);
        console.log('response data ', response.data);
        
        if (response.status === 201) {
          navigation.navigate('DashboardScreenData');
        }
      } else {
        console.log('Tidak semua GR direceive');
      }  */
    }
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "Data Detail Barang Per DO",
    });
  }, [navigation]);

  React.useEffect(() => {
    dispatch(setLoadingTrue());
    let isSubscribed = true;

    fetchDataItemByDO();

    //setPage(0);
    dispatch(setLoadingFalse());
    dispatch(setReceiveAllTrue());
    
    return () => (isSubscribed = false);
  }, []);

  React.useEffect(() => {
    dispatch(setLoadingTrue());
    if (isSwitchAllGoodsReceiveOn){
      dispatch(setReceiveAllTrue());
    } else {
      dispatch(setReceiveAllFalse());
    }
    dispatch(setLoadingFalse());
  }, [isSwitchAllGoodsReceiveOn]);

  return (
    <SafeAreaView style={styles.rootContainer}>
      {/* <View style={styles.imageRowContainer}>
        {route.params?.placeholder_id === "1" ? (
          <Image
            source={{ uri: route.params?.imageUri }}
            style={{ width: 120, height: 120 }}
          />
        ) : (
          <ImagePlaceHolderCameraEmpty
            navigation={navigation}
            route={route}
            image_id="1"
          />
        )}
      </View> */}
      {/* <Divider /> */}
      {/* <View style={{ alignItems: "center", backgroundColor: "white" }}>
        <Title>Barang diterima semua ?</Title>
        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-evenly", alignContent: "space-around" }}>
          <Subheading>Tidak</Subheading>
          <Switch
            value={isSwitchAllGoodsReceiveOn}
            onValueChange={onToggleAllGoodReceiveSwitch}
            style={{ marginHorizontal: 15 }}
          />
          <Subheading>Ya</Subheading>
        </View>
      </View> */}
      {/* <Divider /> */}

      {originalArrayItem ? <DataTableComponents data={originalArrayItem} /> : <View />}

      {/* {
        !recvAllState?
        <Button
        icon="plus"
        mode="contained"
        color="pink"
        //onPress={postDataAndImageGoodsReceive}
        style={styles.buttonDataReturStyle}
      >
        Tambah Data Retur
      </Button>
      : <View />
      }
      <Button
        icon="camera"
        mode="contained"
        onPress={postDataAndImageGoodsReceive}
        style={styles.buttonTakeFotoStyle}
      >
        Simpan Data Pengiriman
      </Button> */}
    </SafeAreaView> 
  );
};

export default KonfirmasiPenerimaanScreen;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: "white",
  },
  imageRowContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 5,
    marginBottom: 20,
  },
  buttonTakeFotoStyle: {
    paddingHorizontal: 10,
    marginHorizontal: 40,
    marginVertical: 5,
  },
  buttonDataReturStyle:{
    paddingHorizontal: 10,
    marginHorizontal: 40,
    marginVertical: 5,
  }
});

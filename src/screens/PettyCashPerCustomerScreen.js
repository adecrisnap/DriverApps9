import { Alert, StyleSheet, View } from "react-native";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import {
  TextInput,
  Headline,
  Subheading,
  useTheme,
  Button,
  Caption,
} from "react-native-paper";
import { Text } from "react-native-paper";
import { currencyFormat } from "../utils/CurrencyFormat";
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import { selectUserId } from "../redux/slices/authSlice";
import {
  FAILED_NETWORK,
  FAILED_SAVE_PETTYCASH_ROUTE,
  GET_PETTY_CASH_EXIST_CUSTOMER,
  NGROK_URL,
  POST_PETTY_CASH_WITH_RUTE,
  showError,
  showSuccess,
  SUCCESS_SAVE_PETTYCASH_ROUTE,
} from "../utils/Constants";
import { useDispatch, useSelector } from "react-redux";

import NetInfo from "@react-native-community/netinfo";
import axios from "axios";

const PettyCashPerCustomerScreen = ({ navigation, route }) => {
  const theme = useTheme();
  const { rute_id } = route.params;
  const dispatch = useDispatch();
  const uid = useSelector(selectUserId);

  const [textParkir, setTextParkir] = React.useState("0");
  const [textBongkar, setTextBongkar] = React.useState("0");
  const [textChecker, setTextChecker] = React.useState("0");
  const [textTarik, setTextTarik] = React.useState("0");
  const [textOther, setTextOther] = React.useState("0");
  const [textPalete, setTextPalete] = React.useState("0");

  const [textParkirDisabled, isTextParkirDisabled] = React.useState(false);
  const [textBongkarDisabled, isTextBongkarDisabled] = React.useState(false);
  const [textCheckerDisabled, isTextCheckerDisabled] = React.useState(false);
  const [textTarikDisabled, isTextTarikDisabled] = React.useState(false);
  const [textOtherDisabled, isTextOtherDisabled] = React.useState(false);
  const [textPaleteDisabled, isTextPaleteDisabled] = React.useState(false);

  const [dataPettyCashExist, isDataPettyCashExist] = React.useState(false);

  const checkDataPettyCashIfAlreadySubmit = React.useCallback(async () => {
    //let dataPettyCashExist = false;

    axios
      .get(
        NGROK_URL +
        GET_PETTY_CASH_EXIST_CUSTOMER +
          "/" +
          rute_id 
      )
      .then(function (response) {
        console.log(response.data.results.data);

        if (response.data?.results.data.length > 0) {
          //dataPettyCashExist = true;
          isTextParkirDisabled(true);
          isTextBongkarDisabled(true);
          isTextCheckerDisabled(true);
          isTextTarikDisabled(true);
          isTextOtherDisabled(true);
          isDataPettyCashExist(true);
          isTextPaleteDisabled(true);

          let parkir = response.data?.results.data[0].PettyCashCL_Parkir;
          let bongkar = response.data?.results.data[0].PettyCashCL_Bongkar;
          let checker = response.data?.results.data[0].PettyCashCL_Checker;
          let tarik = response.data?.results.data[0].PettyCashCL_Tarik;
          let other = response.data?.results.data[0].PettyCashCL_Other;
          let palete = response.data?.results.data[0].PettyCashCL_Pallete;

          setTextParkir(currencyFormat(String(parkir)));
          setTextBongkar(currencyFormat(String(bongkar)));
          setTextChecker(currencyFormat(String(checker)));
          setTextTarik(currencyFormat(String(tarik)));
          setTextOther(currencyFormat(String(other)));
          setTextPalete(currencyFormat(String(palete)));

        } else {
          //dataPettyCashExist = false;

          isTextParkirDisabled(false);
          isTextBongkarDisabled(false);
          isTextCheckerDisabled(false);
          isTextTarikDisabled(false);
          isTextOtherDisabled(false);
          isDataPettyCashExist(false);
          isTextPaleteDisabled(false);
        }
      })
      .catch(function (error) {
        console.log(error);
        //dataPettyCashExist = false;
      });

    //return dataPettyCashExist;
  }, []);

  const onChangeTextParkir = (textParkir) => {
    if (textParkir === "") {
      textParkir = "0";
    }
    setTextParkir(currencyFormat(textParkir));
  };

  const onChangeTextBongkar = (textBongkar) => {
    if (textBongkar === "") {
      textBongkar = "0";
    }
    setTextBongkar(currencyFormat(textBongkar));
  };

  const onChangeTextChecker = (textChecker) => {
    if (textChecker === "") {
      textChecker = "0";
    }
    setTextChecker(currencyFormat(textChecker));
  };

  const onChangeTextTarik = (textTarik) => {
    if (textTarik === "") {
      textTarik = "0";
    }
    setTextTarik(currencyFormat(textTarik));
  };

  const onChangeTextOther = (textOther) => {
    if (textOther === "") {
      textOther = "0";
    }
    setTextOther(currencyFormat(textOther));
  };

  const onChangeTextPalete = (textPalete) => {
    if (textPalete === "") {
      textPalete = "0";
    }
    setTextPalete(currencyFormat(textPalete));
  };

  const savePettyCash = async () => {
    dispatch(setLoadingTrue());

    NetInfo.fetch().then(async (state) => {
      if (state.isConnected == true) {
        const date_iso = new Date().toISOString();
        //console.log(date_iso);
        const response = await axios.post(
          NGROK_URL + POST_PETTY_CASH_WITH_RUTE,
          {
            data: {
              rute_id: rute_id,
              user_id: uid,
              parkir_price: Number.parseFloat(textParkir.replace(/,/g, "")),
              bongkar_price: Number.parseFloat(textBongkar.replace(/,/g, "")),
              checker_price: Number.parseFloat(textChecker.replace(/,/g, "")),
              tarik_price: Number.parseFloat(textTarik.replace(/,/g, "")),
              other_price: Number.parseFloat(textOther.replace(/,/g, "")),
              pallete_price: Number.parseFloat(textOther.replace(/,/g, "")),
              date_isoformat: date_iso,
            },
          }
        );

        if (response.status == 200) {
          showSuccess(SUCCESS_SAVE_PETTYCASH_ROUTE);
          console.log(response.data);
          dispatch(setLoadingFalse());
          navigation.navigate("Dashboard");
          return;
        } else {
          console.log(response.status);
          showError(FAILED_SAVE_PETTYCASH_ROUTE);
          dispatch(setLoadingFalse());
          return;
        }
      } else {
        dispatch(setLoadingFalse());
        //Alert.alert("Network Issue");
        Alert.alert(FAILED_NETWORK);
      }
    });

    dispatch(setLoadingFalse());
  };

  const resetDataPettyCash = async () => {
    setTextParkir("0");
    setTextBongkar("0");
    setTextChecker("0");
    setTextTarik("0");
    setTextOther("0");
    setTextPalete("0");
  };

  const resetPettyCash = async () => {
    await resetDataPettyCash();
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: "Transaksi Petty Cash Customer",
    });
  }, [navigation]);

  React.useEffect(() => {
    checkDataPettyCashIfAlreadySubmit();
  }, []);

  return (
    <SafeAreaView style={[{ backgroundColor: theme?.colors.surface, flex: 1 }]}>
      <View style={styles.container}>
        <Text>
          Rute : {rute_id}
        </Text>

        <View style={styles.input}>
          <Text>Parkir</Text>
          <TextInput
            value={textParkir}
            keyboardType="numeric"
            onChangeText={onChangeTextParkir}
            disabled={textParkirDisabled}
          />
        </View>

        <View style={styles.input}>
          <Text>Bongkar</Text>
          <TextInput
            value={textBongkar}
            keyboardType="numeric"
            onChangeText={onChangeTextBongkar}
            disabled={textBongkarDisabled}
          />
        </View>

        <View style={styles.input}>
          <Text>Checker</Text>
          <TextInput
            value={textChecker}
            keyboardType="numeric"
            onChangeText={onChangeTextChecker}
            disabled={textCheckerDisabled}
          />
        </View>

        <View style={styles.input}>
          <Text style={styles.subhead}>Tarik</Text>
          <TextInput
            value={textTarik}
            keyboardType="numeric"
            onChangeText={onChangeTextTarik}
            disabled={textTarikDisabled}
          />
        </View>

        <View style={styles.input}>
          <Text style={styles.subhead}>Pallete</Text>
          <TextInput
            value={textPalete}
            keyboardType="numeric"
            onChangeText={onChangeTextPalete}
            disabled={textPaleteDisabled}
          />
        </View>

        <View style={styles.input}>
          <Text>Pa' Ogah</Text>
          <TextInput
            value={textOther}
            keyboardType="numeric"
            onChangeText={onChangeTextOther}
            disabled={textOtherDisabled}
          />
        </View>

        {dataPettyCashExist ? (
          <View>
            <Caption style={[{ color: theme?.colors.notification }]}>
              Petty Cash sudah diinput
            </Caption>
          </View>
        ) : (
          <View>
            <Button
              icon="content-save-all-outline"
              mode="outlined"
              onPress={savePettyCash}
            >
              Simpan Data Petty Cash
            </Button>

            <Button
              icon="notification-clear-all"
              mode="contained"
              onPress={resetPettyCash}
            >
              Reset Data
            </Button>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default PettyCashPerCustomerScreen;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  input: {
    marginBottom: 10,
  },
});

import { StyleSheet, View } from 'react-native'
import React from 'react'
import { useDispatch, useSelector } from "react-redux";
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import { selectUserId } from "../redux/slices/authSlice";
import { SafeAreaView } from 'react-native-safe-area-context';
import { Text, 
    TextInput,
    Headline,
    Subheading,
    useTheme,
    Button,
    Caption, } from "react-native-paper";
import { currencyFormat } from '../utils/CurrencyFormat';

import NetInfo from "@react-native-community/netinfo";
import axios from "axios";

import moment from 'moment'
import 'moment/locale/id'
import { FAILED_SAVE_PETTYCASH_ROUTE, GET_PETTY_CASH_EXIST_DAILY, NGROK_URL, POST_PETTY_CASH_DAILY, showError, showSuccess, SUCCESS_SAVE_PETTYCASH_ROUTE } from '../utils/Constants';

const PettyCashPerDeliveryScreen = ({ navigation, route }) => {

    const theme = useTheme();
    const { rute_id } = route.params;
    const dispatch = useDispatch();
    const uid = useSelector(selectUserId);
  
    const [textBensin, setTextBensin] = React.useState("0");
    const [textKenek, setTextKenek] = React.useState("0");
    const [textTol, setTextTol] = React.useState("0");

    const [textBensinDisabled, isTextBensinDisabled] = React.useState(false);
    const [textKenekDisabled, isTextKenekDisabled] = React.useState(false);
    const [textTolDisabled, isTextTolDisabled] = React.useState(false);

    const [dataPettyCashExist, isDataPettyCashExist] = React.useState(false);

    const date= new Date();

    const onChangeTextBensin = (textBensin) => {
        if (textBensin === "") {
            textBensin = "0";
        }
        setTextBensin(currencyFormat(textBensin));
    };

    const onChangeTextKenek = (textKenek) => {
        if (textKenek === "") {
            textKenek = "0";
        }
        setTextKenek(currencyFormat(textKenek));
    };

    const onChangeTextTol = (textTol) => {
        if (textTol === "") {
            textTol = "0";
        }
        setTextTol(currencyFormat(textTol));
    };

    const savePettyCash = async () => {
        dispatch(setLoadingTrue());

        NetInfo.fetch().then(async (state) => {
        if (state.isConnected == true) {
            const date_iso = new Date().toISOString();
            //console.log(date_iso);
            const response = await axios.post(
            NGROK_URL + POST_PETTY_CASH_DAILY,
            {
                data: {
                    rute_id: rute_id,
                    user_id: uid,
                    fuel_price: Number.parseFloat(textBensin.replace(/,/g, "")),
                    kenek_price: Number.parseFloat(textKenek.replace(/,/g, "")),
                    tol_price: Number.parseFloat(textTol.replace(/,/g, "")),
                    date_isoformat: date_iso,
                },
            }
            );

            if (response.status == 200) {
                showSuccess(SUCCESS_SAVE_PETTYCASH_ROUTE);
                console.log(response.data);
                dispatch(setLoadingFalse());
                navigation.navigate("Dashboard");
                return;
            } else {
                console.log(response.status);
                showError(FAILED_SAVE_PETTYCASH_ROUTE);
                dispatch(setLoadingFalse());
                return;
            }
        } else {
            dispatch(setLoadingFalse());
            //Alert.alert("Network Issue");
            Alert.alert(FAILED_NETWORK);
        }
        });

        dispatch(setLoadingFalse());
    }

    const resetPettyCash = async () => {
        setTextBensin("0")
        setTextKenek("0")
        setTextTol("0")
    }


    const checkDataPettyCashIfAlreadySubmitDaily = React.useCallback(async () => {
        axios
          .get(
            NGROK_URL +
            GET_PETTY_CASH_EXIST_DAILY +
              "/" +
              rute_id
          )
          .then(function (response) {
            console.log(response.data.results.data);
            if (response.data?.results.data.length > 0) {
                isTextBensinDisabled(true);
                isTextKenekDisabled(true);
                isTextTolDisabled(true);
                isDataPettyCashExist(true);

                let bensin = response.data?.results.data[0].PettyCashVR_Fuel;
                let kenek = response.data?.results.data[0].PettyCashVR_Kenek;
                let tol = response.data?.results.data[0].PettyCashVR_Tol;

                setTextBensin(currencyFormat(String(bensin)));
                setTextKenek(currencyFormat(String(kenek)));
                setTextTol(currencyFormat(String(tol)));

            } else {
                isTextBensinDisabled(false);
                isTextKenekDisabled(false);
                isTextTolDisabled(false);
                isDataPettyCashExist(false);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
    }, []);

    React.useEffect(() => {
        checkDataPettyCashIfAlreadySubmitDaily();
      }, []);

    React.useLayoutEffect(() => {
        navigation.setOptions({
          title: "Transaksi Petty Cash Harian",
        });
    }, [navigation]);

  return (
    <SafeAreaView style={[{ backgroundColor: theme?.colors.surface, flex: 1 }]}>
        <View style={styles.container}>
             <Text>
               Vehicle : {rute_id}
            </Text> 
            <Text>
                Petty Cash untuk Tanggal : {moment(date).format('LL')}
            </Text>
            
            <View style={styles.input}>
                <Text>SOLAR / BENSIN</Text>
                <TextInput
                    value={textBensin}
                    keyboardType="numeric"
                    onChangeText={onChangeTextBensin}
                    disabled={textBensinDisabled}
                />
            </View>

            <View style={styles.input}>
                <Text>KENEK</Text>
                <TextInput
                    value={textKenek}
                    keyboardType="numeric"
                    onChangeText={onChangeTextKenek}
                    disabled={textKenekDisabled}
                />
            </View>

            <View style={styles.input}>
                <Text>TAP CASH / TOL</Text>
                <TextInput
                    value={textTol}
                    keyboardType="numeric"
                    onChangeText={onChangeTextTol}
                    disabled={textTolDisabled}
                />
            </View>

            {dataPettyCashExist ? (
                <View>
                    <Caption style={[{ color: theme?.colors.notification }]}>
                    Petty Cash sudah diinput
                    </Caption>
                </View> )
                :
                ( <View>
                    <Button
                    icon="content-save-all-outline"
                    mode="outlined"
                    onPress={savePettyCash}
                    >
                    Simpan Data Petty Cash
                    </Button>

                    <Button
                    icon="notification-clear-all"
                    mode="contained"
                    onPress={resetPettyCash}
                    >
                    Reset Data
                    </Button>


                </View>
                )
            }
        </View>
    </SafeAreaView>
  )
}

export default PettyCashPerDeliveryScreen

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
      },
      input: {
        marginBottom: 10,
      },
})
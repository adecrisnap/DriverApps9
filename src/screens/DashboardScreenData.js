import React from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  ScrollView,
  RefreshControl,
} from "react-native";
import {
  Title,
  List,
  FAB,
  Portal,
  useTheme,
  ActivityIndicator,
  Subheading,
  Caption,
  Headline,
} from "react-native-paper";
import axios from "axios";
import {
  BASE_URL,
  GET_PLANNER_CUSTOMERDATA_BY_DELIVERYPLAN,
  NGROK_URL,
} from "../utils/Constants";
import CustomerDeliveryCard from "../components/CustomerDeliveryCard";
import { PreferencesContext } from "../utils/PreferencesContext";
import { useDispatch, useSelector } from "react-redux";
import { selectUserId, selectUserName, setSignOut } from "../redux/slices/authSlice";
import { getDataObjectNonSecured, removeAllKey, storeDataObjectNonSecured } from "../utils/LocalStorage";
import { setLoadingFalse, setLoadingTrue } from "../redux/slices/loadingSlice";
import { setcheckInOutFalse } from "../redux/slices/checkInOutSlice";
import * as TaskManager from 'expo-task-manager';
import { deleteDataStringSecured } from "../utils/SecureLocalStorage";


const Item = ({ title }) => (
  <View style={styles.item}>
    <Title style={styles.title}>{title}</Title>
  </View>
);

const DashboardScreenData = ({ arrayData, navigation, route }) => {

  console.log( 'Array Data', arrayData);

  //console.log('user logged in ', getDataObjectNonSecured('LOGGED IN'));
  
  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);
  const [refreshing, setRefreshing] = React.useState(true);
  const [expanded, setExpanded] = React.useState(true);
  const [customers, setCustomers] = React.useState([]);
  const [state, setState] = React.useState({ open: false });
  const uid = useSelector(selectUserId);

  const onStateChange = ({ open }) => setState({ open });
  const handlePress = () => setExpanded(!expanded);
  const { open } = state;
  const dispatch = useDispatch();
  const username = useSelector(selectUserName);

  //const id_vehicle = React.useState("");

  //const renderItem = ({ item }) => <Item title={item.title} />;

  const openPettyCashDeliveryDaily = async () => {
      navigation.navigate("PettyCashPerDeliveryScreen", {
        rute_id : arrayData[0]?.PlannerDeliveryVR_Number,
      });
  }

  const openReturnNonScheduled = async () => {
    navigation.navigate("ReturnNonScheduled");
  }

  const handleLogout = async () => {
    dispatch(setLoadingTrue());
    dispatch(setcheckInOutFalse());
    await removeAllKey();
    await deleteDataStringSecured('X-TOKEN');
    await TaskManager.unregisterAllTasksAsync();
    dispatch(setLoadingFalse());
    dispatch(setSignOut());
  };

  React.useEffect(() => {
    console.log('REFRESH DATA MANGGIL LAGI');
    let isSubscribed = true;
    //setRefreshing(true);

    const fetchData = async (deliveryPlanId) => {
      await axios
        .get(
          NGROK_URL + GET_PLANNER_CUSTOMERDATA_BY_DELIVERYPLAN + "/" + deliveryPlanId + "/" + uid
        )
        .then(function (response) {
          console.log(response.data.results.data)
          //if (isSubscribed) {
            //storeDataObjectNonSecured("");
            console.log('Refresh Nih');
            setCustomers(response.data.results.data);
          //}
        })
        .catch(function (error) {
          //console.log("ERROR  Delivery Plan ID: ", error);
        });
    };

    //TODO : delivery plan masih static string, harus diprepare dalam bentuk array
    //setDeliveryPlanId("ID20220510111202900");

    //if (deliveryPlanId !== "") {
    //  fetchData(deliveryPlanId).catch(console.error);
    //}

    //console.log('Data2 : ', arrayData);

    {arrayData.map(d => {
      fetchData(d.PlannerDeliveryDP_Number)
        .catch(console.error);
    })}

    const unsubscribe = navigation.addListener('focus', () => {
      isSubscribed = false;
      console.log('masuk UNSUBSCRIBE');
      {arrayData.map(d => {
        console.log('loop aray');
        fetchData(d.PlannerDeliveryDP_Number)
          .catch(console.error);
      })}
    });

     return () => {
    //   // Clear setInterval in case of screen unmount
    //   //clearTimeout(interval);
    //   // Unsubscribe for the focus Listener
       //console.log('masuk return');
       //setRefreshing(true);
       unsubscribe;
     };

    //return () => (isSubscribed = false);
  //}, [deliveryPlanId]);

  }, [navigation]);

  return (
    <SafeAreaView style={[{ backgroundColor: theme?.colors.surface, flex: 1, } ]} >
        <View style={[{ backgroundColor: theme?.colors.surface, marginHorizontal: 10, } ]}>
          <View style={[{ flexDirection: "row" } ]}>
            <View style={[{ flex: 1 } ]}>
              <Title>
                Halo {username}              
              </Title>
            </View>
            <View style={[{ flex: 1 } ]}>
              <Title>
                Petty Cash : Rp {arrayData[0]?.PlannerDeliveryVR_BeginningPettyCash}
              </Title>
            </View>
          </View>
          <Headline>List Antar Hari Ini</Headline>
        </View>
        <FlatList
          data={customers}
          keyExtractor={(item, index) => String(index)}
          renderItem={({ item, index }) => (
            <CustomerDeliveryCard
              id={index}
              key={item.customer_id}
              nama={item.customer_name}
              address={item.customer_address}
              custid={item.customer_id}
              deliveryplanid={item.customer_planid}
              navigation={navigation}
              original_latitude={item.customer_latitude}
              original_longitude={item.customer_longitude}
              status_delivery_id={item.status_delivery_id}
              status_description={item.status_description}
              id_rute={item.rute_number}
              route={route}
            />
          )}
        />

        {/* <Portal> */}
          <FAB.Group
            open={open}
            icon={open ? "calendar-today" : "plus"}
            actions={[
              {
                icon: "wallet-plus-outline",
                label: "Petty Cash Daily",
                onPress: () => openPettyCashDeliveryDaily(),
              },
              {
                icon: "package-up",
                label: "Input Barang Retur Non Terjadwal",
                onPress: () => openReturnNonScheduled(),
              },
              {
                icon: "toggle-switch",
                label: "Dark Mode",
                onPress: () => toggleTheme(),
              },
              {
                icon: "logout",
                label: "Log Out",
                onPress: () => handleLogout(),
              },
            ]}
            onStateChange={onStateChange}
            onPress={() => {
              if (open) {
                // do something if the speed dial is open
              }
            }}
          />
        {/* </Portal> */}
    </SafeAreaView>
  );
};

export default DashboardScreenData;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: theme.colors.accent,
    //marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 14,
  },
  location: {
    //height: 330,
    height: "54%",
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { BASE_URL } from '../utils/Constants'

export const loginApi = createApi({
  reducerPath: 'loginApi',
  baseQuery: fetchBaseQuery({ baseUrl: BASE_URL }),
  tagTypes: [],
  endpoints: (builder) => ({
    postValidateLogin: builder.query({
        query: ({ body }) => ({
            url: `loginapp`,
            method: 'POST',
            body: body, // fetchBaseQuery automatically adds `content-type: application/json` to the Headers and calls `JSON.stringify(patch)`
          }),
    }),
  }),
})

// Export hooks for usage in functional components
export const { usePostValidateLoginQuery } = loginApi
import axios from 'axios';
import { useSelector } from 'react-redux';
import { selectUserToken } from '../redux/slices/authSlice';
import { NGROK_URL } from '../utils/Constants';
import { getDataStringSecured } from '../utils/SecureLocalStorage';



export const axiosInstance = axios.create(
    {
        baseURL: NGROK_URL,
        timeout: 5000,
    }
)

//we intercept every requests 
axiosInstance.interceptors.request.use(async function(config){

  const accessToken = await getDataStringSecured('X-TOKEN');
  if (accessToken) config.headers.Authorization = 'Bearer ' + accessToken;
  console.log(config);
    return config;
}, error => {
    return Promise.reject(error)
})

//we intercept every response
axiosInstance.interceptors.response.use(async function(config){
    return config;
}, error => {
//check for authentication or anything like that
    return Promise.reject(error)
})

export const currencyFormat = (num) => {
  var num1 = Number.parseFloat(num.replace(/,/g, ''));
  return '' + num1.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
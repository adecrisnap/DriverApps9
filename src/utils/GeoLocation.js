import React from 'react';
import * as ExpoLocation from 'expo-location';
import { Platform } from 'react-native';
import { startActivityAsync, ActivityAction } from 'expo-intent-launcher';
import Constants from 'expo-constants'
import { Paragraph, Dialog, Portal } from 'react-native-paper';



const pkg = Constants.manifest.releaseChannel
? Constants.manifest.android.package 
: 'host.exp.exponent'

export const gettingLocationAsync = async () => {

    console.log (pkg);
    try {
        
        await ExpoLocation.enableNetworkProviderAsync().then().catch(_ => null);
        
        let status = await ExpoLocation.hasServicesEnabledAsync();
        //console.log(status)
        if (status){
            let permissionStatus = null;
            let finalStatus = null;
            //console.log(Platform.OS);
            if (Platform.OS === "ios") {
                let { status } = await Permissions.askAsync(Permissions.LOCATION);
                    permissionStatus = status;
            } else {
                
                let { status }  = await ExpoLocation.requestForegroundPermissionsAsync();
                permissionStatus = status;
                console.log('permission ', permissionStatus);
                
            }

            if (permissionStatus !== "granted") {
                let { status } = await ExpoLocation.requestForegroundPermissionsAsync();
                finalStatus = status;
                console.log('final', finalStatus);
                //return true;
            }else{
                return true;
            } 

            if (finalStatus !== "granted") {
                return false;

                //let {resultCode} = await startActivityAsync(ActivityAction.APPLICATION_SETTINGS);
                //console.log('resultCode', resultCode);
            } else {
                return true;
            }
        } else {
            //console.log('masuk sini 4')
            //throw new Error("Please activate the location");
            return false;
        } 
    } catch (e) {
        console.log(e);
        //throw new Error("Please activate the location");
    }
  };
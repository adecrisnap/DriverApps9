import { showMessage } from "react-native-flash-message";

//export const BASE_URL = 'http://10.0.0.160:1338/';
export const BASE_URL = 'http://114.4.131.232:1338/';
export const NGROK_URL = 'http://114.4.131.232:1338/';
//export const NGROK_URL = 'https://9b9f-114-4-131-229.ap.ngrok.io/';
//export const NGROK_URL = 'http://10.0.0.160:1338/';
export const URI_APP_LOGIN = 'loginapp'
export const GET_PLANNER_HEADERID_DELIVERYTODAY_BY_DRIVERID = 'planner_headerid_bydriverid_deliverytoday'
export const GET_PLANNER_CUSTOMERDATA_BY_DELIVERYPLAN = 'planner_customerid_byplanid'
export const GET_PLANNER_ACCUMULATED_SO_ITEM = 'planner_accumulated_soitem_byplanid_and_customerid'
export const GET_PLANNER_SO_NUMBER_BY_DELIVERYPLAN_CUSTOMERID = 'planner_sonumber_byplanid_and_customerid'
export const GET_STATUS_CICO_TODAY = 'status_cico_driver'
export const GET_PETTY_CASH_EXIST_CUSTOMER = 'petty_cash_exist_customer'
export const GET_PETTY_CASH_EXIST_DAILY = 'petty_cash_exist_daily'
export const GET_ALL_CUSTOMERS = 'customers'
export const GET_ALL_PRODUCTS = 'products'
export const GET_RETURNSCHEDULED_NOT_PROCESSED = 'return_scheduled_notprocessed_bycustomerid'
export const GET_RETURNSCHEDULEDID_NOT_PROCESSED = 'return_scheduledid_notprocessed_bycustomerid'
export const GET_APP_VERSION = 'appversion'

export const POST_LOCATION_DATA = 'post_checkinout'
export const POST_GOODS_RECEIVE = 'post_goods_receive'
export const POST_PETTY_CASH_WITH_RUTE = 'post_petty_cash_with_rute'
export const POST_IMAGE = 'upload'
export const POST_PETTY_CASH_DAILY = 'post_petty_cash_daily'

export const POST_RETUR_NON_SCHEDULED = 'post_return_nonscheduled'
export const POST_RETUR_SCHEDULED = 'post_return_scheduled'

//export const GET_CUSTOMER_DATA_BY_DELIVERY_PLAN = 'getcustomerdata_bydeliveryplan'
//export const GET_DONUMBER_BY_PLAN_AND_CUSTOMER = 'getdonumberbyplanandcustomerid'
//export const GET_DONUMBER_BY_PLAN_AND_CUSTOMER = 'getdonumberbyplanandcustomerid'


//export const POST_LOCATION_DATA = 'postlocationdata'
//export const GET_DATAITEM_BY_DO = 'getdataitem_bydo'
//export const POST_IMAGE = 'upload'
//export const POST_GOODS_RECEIVE_DATA = 'postgoodreceive'
//export const GET_STATUS_CHECKINOUT_DRIVER = 'getstatus_checkinoutdriver'

export const INVALID_LOGIN = 'Gagal Login';
export const NO_USERNAME_PASSWORD_INPUT = 'Username / password wajib diisi'
export const NO_DATA_FOUND = 'Data tidak ditemukan'
export const USER_ALREADY_CHECKOUT = 'User sudah checkout'
export const USER_ALREADY_CHECKIN = 'User sedang check-in di tempat lain'
export const GEOLOCATION_DATA_NOT_REAL = 'GPS Mocked Data Found. Please try again'
export const POST_DATA_GOODS_RECEIVE_FAILED = 'Gagal Simpan Data. Harap Coba Lagi'
export const PICTURE_NOT_AVAILABLE = 'Gambar tidak tersedia'
export const RETUR_DATA_NOT_EXISTS = 'Data Retur belum diinput'
export const FAILED_IMAGE_UPLOAD = 'Gagal Upload Image'

export const SUCCESS_CHECKIN = 'Sukses Check In'
export const FAILED_CHECKIN = 'Gagal Check In'

export const SUCCESS_CHECKOUT = 'Sukses Check Out'
export const FAILED_CHECKOUT = 'Gagal Check Out'

export const SUCCESS_SAVE_GR = 'Sukses Simpan Data Serah Terima Barang'
export const FAILED_SAVE_GR = 'Gagal Simpan Data Serah Terima Barang'

export const SUCCESS_SAVE_PETTYCASH_ROUTE = 'Sukses Simpan Petty Cash'
export const FAILED_SAVE_PETTYCASH_ROUTE = 'Gagal Simpan Petty Cash'

export const SUCCESS_SAVE_RN = 'Sukses Simpan Data Retur'
export const FAILED_SAVE_RN = 'Gagal Simpan Data Retur'


export const FAILED_NETWORK = 'Jaringan gagal. Coba Kembali'


export const STATUS_DO_INIT = 'Belum Dikirim'
export const STATUS_DO_SUCCES_SEND = 'Sudah Dikirim'
export const STATUS_DO_FAILED_SEND = 'Gagal Dikirim'

export const LOCATION_START_TASK_NAME = 'BACKGROUND-LOCATION-TASK-START'
export const LOCATION_STOP_TASK_NAME = 'BACKGROUND-LOCATION-TASK-STOP'

export const PROGRAM_NAME = 'BPR_MobileDriver'

export const showError = (message) => {
    showMessage({
      message: message,
      type: "danger",
    });
  };

export const showSuccess = (message) => {
    showMessage({
      message: message,
      type: "success",
    });
  };

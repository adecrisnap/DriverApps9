import AsyncStorage from "@react-native-async-storage/async-storage";
import { showError } from "../utils/Constants";

export const storeDataStringNonSecured = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    //console.log(e);
  }
};

export const storeDataObjectNonSecured = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    //console.log(e);
  }
};

export const getDataStringNonSecured = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    } else {
      return null;
    }
  } catch (e) {
    //console.log(e);
  }
};

export const getDataObjectNonSecured = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    //console.log(e);
  }
};

export const getFilterAsyncStorage = async (filter) => {
  try {
    const result = {};
    const mapResult = [];
    const keys = await AsyncStorage.getAllKeys();
    //const items = await AsyncStorage.multiGet(keys);
    for (const key of keys) {
      if (key.startsWith(filter)){
        const val = await AsyncStorage.getItem(key);
        result[key] = val;
        mapResult.push(JSON.parse(val));
      }
    }
    //return items;
    return mapResult;
  } catch (e) {
    //console.log(e);
  }
};

export const removeFilterAsyncStorage = async (filter) => {
  try {
    //const result = {};
    const keys = await AsyncStorage.getAllKeys();
    //const items = await AsyncStorage.multiGet(keys);
    for (const key of keys) {
      if (key.startsWith(filter)){
        await AsyncStorage.removeItem(key);
      }
    }

    //return items;
    //return result;
  } catch (e) {
    //console.log(e);
  }
};

export const removeAllKey = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    //console.log(e);
  }
};

export const removeKey = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    //console.log(e);
  }
};

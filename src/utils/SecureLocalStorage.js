import * as SecureStore from 'expo-secure-store';


export const storeDataStringSecured = async (key, value) => {
    await SecureStore.setItemAsync(key, value);
}

export const getDataStringSecured = async (key) => {
    try {
        const value = await SecureStore.getItemAsync(key);
        if (value !== null) {
          return value;
        } else {
          return null;
        }
      } catch (e) {
        //console.log(e);
      }
}

export const deleteDataStringSecured = async(key) => {
  try {
    await SecureStore.deleteItemAsync(key, options)
  } catch (e) {
    //console.log(e);
  }
}
  
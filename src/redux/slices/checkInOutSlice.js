import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isAlreadyCheckInSomewhere: false
}

const checkInOutSlice = createSlice({
    name: 'checkInOut',
    initialState,
    reducers: {
        setcheckInOutTrue: (state) => {
            state.isAlreadyCheckInSomewhere = true;
        },
        setcheckInOutFalse: (state) => {
            state.isAlreadyCheckInSomewhere = false;
        }
    }
});

export const { setcheckInOutTrue, setcheckInOutFalse } = checkInOutSlice.actions;

export const selectIsAlreadyCheckInSomewhere = (state) => state.checkInOut.isAlreadyCheckInSomewhere;

export default checkInOutSlice.reducer;
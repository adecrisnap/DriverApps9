import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false
}

const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers: {
        setLoading: (state, action) => {
            state.isLoading = action.payload.isLoading;
        },
        setLoadingTrue: (state) => {
            state.isLoading = true;
        },
        setLoadingFalse: (state) => {
            state.isLoading = false;
        }
    }
});

export const { setLoading, setLoadingFalse, setLoadingTrue } = loadingSlice.actions;

export const selectIsLoading = (state) => state.loading.isLoading;

export default loadingSlice.reducer;
import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    isLoggedIn: false,
    userName: null,
    userId: null,
    userToken: null,
}

const authSlice = createSlice({
    name: 'userAuth',
    initialState,
    reducers: {
        setSignIn: (state, action) => {

            state.isLoggedIn = action.payload.isLoggedIn;
            state.userName = action.payload.userName;
            state.userId = action.payload.userId;
            state.userToken = action.payload.userToken;

        },
        setSignOut: (state) => {
            state.userName = null;
            state.isLoggedIn = false;
            state.userId = null;
            state.userToken = null;
            }
    }
});

export const { setSignIn, setSignOut } = authSlice.actions;

export const selectIsLoggedIn = (state) => state.userAuth.isLoggedIn;
export const selectUserName = (state) => state.userAuth.userName;
export const selectUserId = (state) => state.userAuth.userId;
export const selectUserToken = (state) => state.userAuth.userToken;


export default authSlice.reducer;

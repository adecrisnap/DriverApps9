import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isReceiveAll: false
}

const receiveSlice = createSlice({
    name: 'receive',
    initialState,
    reducers: {
        setReceiveAllTrue: (state) => {
            state.isReceiveAll = true;
        },
        setReceiveAllFalse: (state) => {
            state.isReceiveAll = false;
        }
    }
});

export const { setReceiveAllTrue, setReceiveAllFalse } = receiveSlice.actions;

export const selectIsReceiveAll = (state) => state.receive.isReceiveAll;

export default receiveSlice.reducer;
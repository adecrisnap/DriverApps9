import { configureStore, combineReducers } from '@reduxjs/toolkit'
import authSlice from './slices/authSlice'
import checkInOutSlice from './slices/checkInOutSlice'
import loadingSlice from './slices/loadingSlice'
import receiveSlice from './slices/receiveSlice'

export const store = configureStore({
  reducer: {
    userAuth: authSlice,
    loading: loadingSlice,
    receive: receiveSlice,
    checkInOut: checkInOutSlice,
  },
}) 
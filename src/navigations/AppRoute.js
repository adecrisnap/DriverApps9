import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { selectIsLoggedIn, setSignIn, setSignOut } from '../redux/slices/authSlice';
import AppNavigator from './AppNavigator';
import AuthNavigator from './AuthNavigator';
import { useSelector } from 'react-redux';
import { selectIsLoading, setLoadingFalse, setLoadingTrue } from '../redux/slices/loadingSlice';
import LoadingScreen from '../screens/LoadingScreen';
import FlashMessage from "react-native-flash-message";
import { useKeepAwake } from 'expo-keep-awake';
import { getDataObjectNonSecured, removeAllKey } from '../utils/LocalStorage';
import { gettingLocationAsync } from '../utils/GeoLocation';
import { useDispatch } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as ExpoLocation from "expo-location";
import * as TaskManager from 'expo-task-manager';
import { GET_APP_VERSION, LOCATION_START_TASK_NAME, NGROK_URL, PROGRAM_NAME, showError } from '../utils/Constants';
import { Button, Paragraph, Dialog, Portal, Provider } from 'react-native-paper';
import { startActivityAsync, ActivityAction } from 'expo-intent-launcher';
import { setcheckInOutFalse } from '../redux/slices/checkInOutSlice';
import { deleteDataStringSecured, storeDataStringSecured } from '../utils/SecureLocalStorage';
import * as Application from 'expo-application';
import axios from 'axios';
import compareVersions from 'compare-versions';
import { Alert, BackHandler } from 'react-native';
import Constants from "expo-constants"

const AppRoute = () => {
    //const [isLoggedIn, setIsLoggedIn] = React.useState(false);
    const dispatch = useDispatch();
    const [sameVer, isSameVer] = React.useState(false);
    const [statusPermission, setStatusPermission] = ExpoLocation.useForegroundPermissions();
    let resultCodeDialog = -1;

    //let appVersionInternal = Application.nativeApplicationVersion;
    //let currentLogin = false;

    //const user = getDataObjectNonSecured('LOGGED IN');
    //console.log('User Logged In', user);
    let isLoggedIn = useSelector(selectIsLoggedIn);
    const loadingState = useSelector(selectIsLoading);
    //console.log("Loading Root :", loadingState)
    //useKeepAwake();
    const [visible, setVisible] = React.useState(false);

    const showDialog = async() => {
        setVisible(true);
        //let {resultCode} = await startActivityAsync(ActivityAction.APPLICATION_SETTINGS);
    }

    const openSystem = async() => {
        //setVisible(false);
        let { resultCode } = await startActivityAsync(ActivityAction.APPLICATION_SETTINGS);
        console.log('result code', resultCode);
        resultCodeDialog = resultCode;
        await hideDialog();
    }

    const hideDialog = async() => {
        setVisible(false);
    }

    const showAlertMismatchVersion = async () => { return new Promise((resolve, reject) => {
        Alert.alert(
          "Update Program",
          "Harap Update Program",
          [
            {
              text: "OK", 
              onPress: () => {
                resolve(true)
              },
            }
          ],
          {
            cancelable: false,
            onDismiss: () => {
              resolve (true)
            },
          }
        )
      });
    }

    const checkLoggedInAsync = async() => {
        try{
            //console.log(isLoggedIn);
            const currentUser = await AsyncStorage.getItem('@MyApp_user')
            const user1 = JSON.parse(currentUser);
            
            if ( user1 != null ){
                const user = {
                    isLoggedIn: user1.isLoggedIn,
                    userName: user1.userName,
                    userId: user1.userId,
                    userToken: user1.userToken,
                    loggin_date: user1.loggin_date,
                }

                var today_date = new Date();
                var login_date = new Date(user.loggin_date);
                console.log(today_date.getUTCDate());
                console.log(login_date.getUTCDate());

                if (today_date.getUTCDate() === login_date.getUTCDate() )
                {
                    //console.log('masih hari yang sama');

                    // await ExpoLocation.startLocationUpdatesAsync(LOCATION_START_TASK_NAME, {
                    //     accuracy: ExpoLocation.Accuracy.Highest,
                    //   });
                    storeDataStringSecured('X-TOKEN',user.userToken );
                    dispatch(setSignIn(user));
                } else {
                    dispatch(setLoadingTrue());
                    dispatch(setcheckInOutFalse());
                    await removeAllKey();
                    await deleteDataStringSecured('X-TOKEN');
                    dispatch(setLoadingFalse());
                    dispatch(setSignOut());
                }
                //dispatch(setSignIn(user));
            } else {
                
            }
        } catch(e){
            console.log(e);
        }
    }


    React.useEffect(() => {
        //console.log('masuk awal');
        const bootstrapAsync = async () => {
            //console.log('Permission : ', statusPermission?.granted);
            if (statusPermission?.granted){
                let status = await gettingLocationAsync();
                //console.log('status ',status);
                if(status){
                    let ifAlreadyLoggedIn = await checkLoggedInAsync();
                } else  {
                   // throw new Error("Please activate the location");
                   // showDialog();
                }
            } else {
                let status = await gettingLocationAsync();
                console.log('status ',status);
                if(status){
                    let ifAlreadyLoggedIn = await checkLoggedInAsync();
                } else  {
                   // throw new Error("Please activate the location");
                    showDialog();
                }
            }
            
        }

        const checkingVersion = async () => {

            let appDbVersionObject =  await axios.get(NGROK_URL + GET_APP_VERSION + '/' + PROGRAM_NAME)
            let appDbVersion = appDbVersionObject.data.results.data[0].AppProgram_Version;
            let appVersionInternal = Constants.manifest.version;

            let intCompare = compareVersions(appDbVersion,appVersionInternal);
            
            console.log("appDbVersion : ", appDbVersion)
            console.log("appVersionInternal : ", appVersionInternal)
            console.log("intCompare : ", intCompare)
            
            //return intCompare;

            if (intCompare == -1 ){
                let mismatch = await showAlertMismatchVersion();
                if (mismatch){
                    BackHandler.exitApp();
                }
            } else {
                if (intCompare == 1 ) {
                    let mismatch = await showAlertMismatchVersion();
                    if (mismatch){
                        BackHandler.exitApp();
                    }
                } else {
                    bootstrapAsync();
                }
            }
        }
        
        checkingVersion();
        //console.log("checkVer2 : ", checkVer2)
        
        //bootstrapAsync();
    }, [])


    return (
        // <NavigationContainer>
            <>
            {
                //isLoggedIn ? <AppNavigator /> : <AuthNavigator />
                !isLoggedIn ?
                <> 
                <AuthNavigator />
                <Portal>
                    <Dialog visible={visible} onDismiss={hideDialog}>
                    <Dialog.Content>
                        <Paragraph>To Continue, let your device turn on location</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={openSystem}>Ok</Button>
                    </Dialog.Actions>
                    </Dialog>
                </Portal>
                </> 
                : 
                <AppNavigator /> 
                //user ? isLoggedIn ? <AppNavigator /> : <AuthNavigator />
            }
            
            <FlashMessage position="top" /> 
            {loadingState && <LoadingScreen />} 
            </>
        //  </NavigationContainer>
    )

}

export default AppRoute
import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import DashboardScreen from '../screens/DashboardScreen';
import DashboardScreenData from '../screens/DashboardScreenData';
import KonfirmasiPenerimaanScreen from '../screens/KonfirmasiPenerimaanScreen';
import CameraPreviewScreen from '../screens/CameraPreviewScreen';
import TransaksiTokoScreen from '../screens/TransaksiTokoScreen';
import RekapPengirimanBarangScreen from '../screens/RekapPengirimanBarangScreen';
import PettyCashPerCustomerScreen from '../screens/PettyCashPerCustomerScreen';
import PettyCashPerDeliveryScreen from '../screens/PettyCashPerDeliveryScreen';
import ReturnNonScheduled from '../screens/ReturnNonScheduled';
import ReturScreen from '../screens/ReturScreen';


const Stack = createNativeStackNavigator();

const AppNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='Dashboard' >
            <Stack.Screen name="Dashboard" component={DashboardScreen}  />
            <Stack.Screen name="DashboardScreenData" component={DashboardScreenData} />
            <Stack.Screen name="KonfirmasiPenerimaanScreen" component={KonfirmasiPenerimaanScreen} />
            <Stack.Screen name="CameraPreviewScreen" component={CameraPreviewScreen} />
            <Stack.Screen name="TransaksiTokoScreen" component={TransaksiTokoScreen} />
            <Stack.Screen name="RekapPengirimanBarangScreen" component={RekapPengirimanBarangScreen} />
            <Stack.Screen name="PettyCashPerCustomerScreen" component={PettyCashPerCustomerScreen} />
            <Stack.Screen name="PettyCashPerDeliveryScreen" component={PettyCashPerDeliveryScreen} />
            <Stack.Screen name="ReturnNonScheduled" component={ReturnNonScheduled} />
            <Stack.Screen name="ReturScreen" component={ReturScreen} />
        </Stack.Navigator>
    )
}

export default AppNavigator
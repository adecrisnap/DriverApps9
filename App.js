import 'react-native-gesture-handler';
import React from 'react';
import { Provider as PaperProvider, DarkTheme as PaperDarkTheme, DefaultTheme as PaperDefaultTheme } from 'react-native-paper';
import { NavigationContainer, DarkTheme as NavigationDarkTheme, DefaultTheme as NavigationDefaultTheme } from '@react-navigation/native';
import { Provider as StoreReduxProvider } from 'react-redux';
//import Main from './src/Main';
import { store } from './src/redux/store';
import AppRoute from './src/navigations/AppRoute';
import { PreferencesContext } from './src/utils/PreferencesContext'
import * as ExpoLocation from "expo-location";
import * as TaskManager from 'expo-task-manager';
import { LOCATION_START_TASK_NAME, LOCATION_STOP_TASK_NAME } from './src/utils/Constants';

//import { setupStore } from './src/redux/store';

const CombinedDarkTheme = {
  ...PaperDarkTheme,
  ...NavigationDarkTheme,
  colors: { ...PaperDarkTheme.colors, ...NavigationDarkTheme.colors },
};

const CombinedDefaultTheme = {
  ...PaperDefaultTheme,
  ...NavigationDefaultTheme,
  colors: {
    ...PaperDefaultTheme.colors,
    ...NavigationDefaultTheme.colors,
  },
};

//const store = setupStore();

export default function App() {

  const [isThemeDark, setIsThemeDark] = React.useState(false);
  //const [status, requestPermission] = ExpoLocation.useForegroundPermissions();

  let theme = isThemeDark ? CombinedDarkTheme : CombinedDefaultTheme;

  const toggleTheme = React.useCallback(() => {
    return setIsThemeDark(!isThemeDark);
  }, [isThemeDark]);

  const preferences = React.useMemo(
    () => ({
      toggleTheme,
      isThemeDark,
    }),
    [toggleTheme, isThemeDark]
  );

  TaskManager.defineTask(LOCATION_START_TASK_NAME, async ({ data, error }) => {
    if (error) {
        console.log(error);
        return;
    }

    if (data) {
      console.log(data);
    }
  });


  TaskManager.defineTask(LOCATION_STOP_TASK_NAME, async ({ data, error }) => {
    if (error) {
        console.log(error);
        return;
    }

    if (data) {
      console.log(data);
    }
  });

  return (
    <PreferencesContext.Provider value={preferences}>
      <StoreReduxProvider store={store}>
        <PaperProvider theme={theme}>
          <NavigationContainer theme={theme}>
            <AppRoute />
          </NavigationContainer>
        </PaperProvider>
      </StoreReduxProvider>
    </PreferencesContext.Provider>
  );
}